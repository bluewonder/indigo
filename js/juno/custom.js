jQuery(document).ready(function($) {

	/*homepage*/
	jQuery('#nav-ul .mega-menu > .inner').hide();
        jQuery('#nav-ul > li').hoverIntent({
            over:function(){
                /*First*/
                jQuery(this).addClass('active');
                jQuery(this).find('.item-bg').show();
                jQuery(this).find('.mega-menu > .inner').show();
                /*change opacity of header .header-part-2 .opacity-div*/
                jQuery('header .header-part-2 .opacity-div,.breadcrumb-wide').animate({opacity:1}, 500);
                /*add White bg*/
                var InnerMenu = jQuery('#nav-ul .inner').outerHeight();
                var InnerMenu = InnerMenu ;
                jQuery(this).find('.mega-menu').append('<div class="white-bg" style="height:'+InnerMenu+'px"></div>');

                var TopHeight = jQuery(this).find('.mega-menu .links-column');
                var SetHeight = jQuery(this).find('.mega-menu .mega-product');
                if(SetHeight.get(0) ) {
                    SetHeight.css('min-height', (TopHeight.outerHeight())+'px');
                }
                
            },
            timeout:100,//milliseconds
            out:function(){
                jQuery(this).removeClass('active');
                jQuery(this).find('.item-bg').hide();
                jQuery(this).find('.mega-menu > .inner').hide();
                /*change opacity of header .header-part-2 .opacity-div*/
                /*jQuery('header .header-part-2 .opacity-div').animate({opacity:.85}, 500);*/
                /*remove White bg*/
                jQuery(this).find('.mega-menu .white-bg').remove();
            }

        });
        jQuery('#nav-ul').hoverIntent({
            over:function(){
            },
            timeout:200,//milliseconds 
            out:function(){
            	if(jQuery('.cms-home')[0]) {
                		jQuery('header .header-part-2 .opacity-div').animate({opacity:.85}, 500);
                	}else{
                		jQuery('header .header-part-2 .opacity-div,.breadcrumb-wide').animate({opacity:.95}, 500);
                	}
            }

        });


        function stickyNav() {
            var scrollTop = jQuery(window).scrollTop();
            //console.log(scrollTop);
            var header_top_h = jQuery('.header-part-2').outerHeight();
            if(scrollTop>109){
                jQuery('.header-part-2').addClass('sticky');
               
            }else{
                jQuery('.header-part-2').removeClass('sticky');
            }
        }

        stickyNav();
        jQuery(window).scroll(function(event) {
            stickyNav();
        });

       /*product page*/

	jQuery('.button2.up').click(function(event) {
		/* Act on the event */
		var current_qty  = parseInt(jQuery(this).prev('.input-text.qty').val());
		 if (!current_qty || current_qty=="" || current_qty == "NaN" || current_qty < 0) current_qty = 0;
		jQuery(this).prev('.input-text.qty').val(++current_qty);
		event.preventDefault();
	});		
	

	jQuery('.button2.down').click(function(event) {
		/* Act on the event */
		var current_qty  = parseInt(jQuery(this).next('.input-text.qty').val());
		if (!current_qty || current_qty=="" || current_qty == "NaN") current_qty = 0;
        if(current_qty>0){
		  jQuery(this).next('.input-text.qty').val(--current_qty);
        }else{
            alert("Please Input A Valid Item's Number")
        }
		event.preventDefault();
	});

	//Product images
	/*function AddHeightWidth(){
		var img_height = jQuery('.product-image-gallery img').height();
		var img_width = jQuery('#left-product').width();
		jQuery('.product-image').height(img_height);
		jQuery('#magento-media > .inner').width(img_width);
	}
	AddHeightWidth()
	jQuery(window).resize(function() {
		  AddHeightWidth();
	});*/

	//Product tab radio
	jQuery('.fed-select-container ul.size-list li').each(function() {
		jQuery(this).find('img').click(function(event) {
			/* Act on the event */
			jQuery(this).next('.radio-box').find('input[type="radio"]').trigger( "click" );
		});

		if(jQuery(this).find('.radio-box input[type="radio"]').is(':checked')){
			jQuery(this).addClass('active');
		}
		jQuery(this).find('.radio-box input[type="radio"]').change(function(event) {
			/* Act on the event */
			if(jQuery(this).is(':checked')){
				jQuery('.fed-select-container li').removeClass('active');
				jQuery(this).parentsUntil('ul.size-list').addClass('active');
			}
		});
	});


	jQuery('ul.item-collapse li').click(function(){
	    if(jQuery(this).find('.content').is(":hidden")){
            jQuery('ul.item-collapse li .content').slideUp('fast');
            jQuery('ul.item-collapse li span.right').text("+");
            jQuery(this).find('.content').slideDown('fast');
            jQuery(this).find('span.right').text("-");
            jQuery('ul.item-collapse li').removeClass('active')
            jQuery(this).addClass('active');
	    }else{
            jQuery(this).find('.content').slideUp('fast');
            jQuery(this).find('span.right').text("+");
            jQuery(this).removeClass('active');
	    }
	});
    jQuery('ul.item-collapse li:first').trigger("click");

    /*three-column height*/
    jQuery('.three-column .col').each(function(index) {
	var max_height = [];
	max_height[index] = jQuery(this).outerHeight();
	var max_height_1 = Math.max.apply(Math, max_height);
	console.log(max_height_1);  	
    });

    /*Add Contact Page Padding*/
    function AddContacPadding(){
            //window sizing:
            var windowsize = $(window).width();
	    	if ($(".full-screen-banner")[0]){
                if(!$("#category-top")[0]){
                    var banner_height = jQuery('.col-main .full-screen-banner img.base_img').outerHeight()+40;
                }
	        jQuery('.col-main').css('padding-top',banner_height+'px');
	        /*jQuery('.full-screen-banner .text-wrapper').css({'margin-top':-(banner_height+40)/2+'px'});*/
	        jQuery('.breadcrumbs').css({
	        	position: 'absolute',
	        	width: '100%',
	        	top:banner_height-40+'px'
	        });
	    	}
	}

    //call functions :
    AddContacPadding();

    /*Resize*/
    jQuery(window).resize(function() {
        AddContacPadding();
    });
    /*showroom slider*/
    jQuery('.full-screen-banner .bxslider').bxSlider({
    		mode:'fade',
    		pagerCustom: '#bx-pager',
    		speed:1000,
    		controls:false
    });
    /*about single*/
      jQuery('.top-tight-banner .bxslider').bxSlider({
    		mode:'fade',
    		controls:false,
            auto:true

    });
      /*top responsive menu*/
        var menu_button = jQuery('#menu-res');
        var res_menu = jQuery('header .nav-wrapper');
        jQuery('#menu-res').click(function(event) {
            /* Act on the event */
            if(res_menu.is(":hidden")){
                res_menu.slideDown();
            }else{
                res_menu.slideUp();
            }
        });
        /*search responsive */
         jQuery('#search_mini_form_2 input').focus(function(){
          if(jQuery(this).val()=='Search Indigo'){
              jQuery(this).val('');
          }
         }).blur(function(){
          if(jQuery(this).val()==''){
              jQuery(this).val('Search Indigo');
          }
      });

});







