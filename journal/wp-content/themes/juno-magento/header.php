<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/fancybox/jquery.fancybox-1.3.4.css" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
 $block = Mage::getSingleton('core/layout');
 $head = $block -> createBlock('page/html_head');
 $head -> addCss('css/styles.css');
 $head -> addCss('css/local.css');
 $head -> addJs('prototype/prototype.js');
 $head -> addJs('prototype/validation.js');
 $head -> addJs('varien/js.js');
 $head -> addJs('varien/form.js');
 $head -> addJs('varien/menu.js');
echo $head -> getCssJsHtml();
?>

<script type="text/javascript" src="<?php echo Mage::getDesign()->getSkinUrl('js/jquery-1.8.2.js') ?>"></script>
<script type="text/javascript" src="<?php echo Mage::getDesign()->getSkinUrl('js/hoverIntent.js') ?>"></script>
<script type="text/javascript" src="<?php echo Mage::getDesign()->getSkinUrl('js/jquery.noConflict.js') ?>"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery.fancybox-1.3.4.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery.tweet.js"></script>

<script type="text/javascript">
    jQuery(function($){
        $(".tweet").tweet({
          username: "LilyGardner",
          count: 3,         
          loading_text: "loading tweets..."
        });
    });
</script>

<script type="text/javascript">
//<![CDATA[
Mage.Cookies.path     = '/s/sahara/dev';
Mage.Cookies.domain   = '.dev.junowebdesign.com';
//]]>
</script>

<script type="text/javascript">//<![CDATA[
        var Translator = new Translate([]);
        //]]></script>
<script type="text/javascript" src="//use.typekit.net/auo4ggc.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<?php
    
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	wp_head();
?>
</head>
<body <?php body_class('journal'); ?>>
    <div class="wrapper">
	<div class="page">
	<?php
	$header = $block -> createBlock('page/html_header');
	$header -> setTemplate('page/html/header.phtml');
	
	if ($topMenu = $block -> createBlock('page/html_topmenu')) {
		$topMenu -> setTemplate('page/html/topmenu.phtml');
	}
	
	$topSearch = $block -> createBlock('core/template');
	$topSearch -> setTemplate('catalogsearch/form.mini.phtml');
	
	$topCurrency = $block -> createBlock("directory/currency");
	$topCurrency -> setTemplate('directory/currency.phtml');
	
	$header -> setChild('topSearch', $topSearch);	
	$header -> setChild('topMenu', $topMenu);
	$header -> setChild('topCurrency', $topCurrency);
	echo $header -> toHTML();
	?>