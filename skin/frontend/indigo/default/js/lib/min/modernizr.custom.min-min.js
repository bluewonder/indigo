window.Modernizr=function(e,t,n){function r(e){m.cssText=e}function o(e,t){return r(v.join(e+";")+(t||""))}function i(e,t){return typeof e===t}function a(e,t){return!!~(""+e).indexOf(t)}function c(e,t,r){for(var o in e){var a=t[e[o]];if(a!==n)return r===!1?e[o]:i(a,"function")?a.bind(r||t):a}return!1}var l="2.6.2",u={},s=!0,f=t.documentElement,d="modernizr",p=t.createElement(d),m=p.style,h,y={}.toString,v=" -webkit- -moz- -o- -ms- ".split(" "),g={},b={},E={},S=[],C=S.slice,j,w=function(e,n,r,o){var i,a,c,l,u=t.createElement("div"),s=t.body,p=s||t.createElement("body");if(parseInt(r,10))for(;r--;)c=t.createElement("div"),c.id=o?o[r]:d+(r+1),u.appendChild(c);return i=["&#173;",'<style id="s',d,'">',e,"</style>"].join(""),u.id=d,(s?u:p).innerHTML+=i,p.appendChild(u),s||(p.style.background="",p.style.overflow="hidden",l=f.style.overflow,f.style.overflow="hidden",f.appendChild(p)),a=n(u,e),s?u.parentNode.removeChild(u):(p.parentNode.removeChild(p),f.style.overflow=l),!!a},N=function(t){var n=e.matchMedia||e.msMatchMedia;if(n)return n(t).matches;var r;return w("@media "+t+" { #"+d+" { position: absolute; } }",function(t){r="absolute"==(e.getComputedStyle?getComputedStyle(t,null):t.currentStyle).position}),r},x={}.hasOwnProperty,T;T=i(x,"undefined")||i(x.call,"undefined")?function(e,t){return t in e&&i(e.constructor.prototype[t],"undefined")}:function(e,t){return x.call(e,t)},Function.prototype.bind||(Function.prototype.bind=function(e){var t=this;if("function"!=typeof t)throw new TypeError;var n=C.call(arguments,1),r=function(){if(this instanceof r){var o=function(){};o.prototype=t.prototype;var i=new o,a=t.apply(i,n.concat(C.call(arguments)));return Object(a)===a?a:i}return t.apply(e,n.concat(C.call(arguments)))};return r}),g.touch=function(){var n;return"ontouchstart"in e||e.DocumentTouch&&t instanceof DocumentTouch?n=!0:w(["@media (",v.join("touch-enabled),("),d,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(e){n=9===e.offsetTop}),n},g.localstorage=function(){try{return localStorage.setItem(d,d),localStorage.removeItem(d),!0}catch(e){return!1}};for(var F in g)T(g,F)&&(j=F.toLowerCase(),u[j]=g[F](),S.push((u[j]?"":"no-")+j));return u.addTest=function(e,t){if("object"==typeof e)for(var r in e)T(e,r)&&u.addTest(r,e[r]);else{if(e=e.toLowerCase(),u[e]!==n)return u;t="function"==typeof t?t():t,"undefined"!=typeof s&&s&&(f.className+=" "+(t?"":"no-")+e),u[e]=t}return u},r(""),p=h=null,function(e,t){function n(e,t){var n=e.createElement("p"),r=e.getElementsByTagName("head")[0]||e.documentElement;return n.innerHTML="x<style>"+t+"</style>",r.insertBefore(n.lastChild,r.firstChild)}function r(){var e=v.elements;return"string"==typeof e?e.split(" "):e}function o(e){var t=h[e[p]];return t||(t={},m++,e[p]=m,h[m]=t),t}function i(e,n,r){if(n||(n=t),y)return n.createElement(e);r||(r=o(n));var i;return i=r.cache[e]?r.cache[e].cloneNode():f.test(e)?(r.cache[e]=r.createElem(e)).cloneNode():r.createElem(e),i.canHaveChildren&&!s.test(e)?r.frag.appendChild(i):i}function a(e,n){if(e||(e=t),y)return e.createDocumentFragment();n=n||o(e);for(var i=n.frag.cloneNode(),a=0,c=r(),l=c.length;l>a;a++)i.createElement(c[a]);return i}function c(e,t){t.cache||(t.cache={},t.createElem=e.createElement,t.createFrag=e.createDocumentFragment,t.frag=t.createFrag()),e.createElement=function(n){return v.shivMethods?i(n,e,t):t.createElem(n)},e.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+r().join().replace(/\w+/g,function(e){return t.createElem(e),t.frag.createElement(e),'c("'+e+'")'})+");return n}")(v,t.frag)}function l(e){e||(e=t);var r=o(e);return v.shivCSS&&!d&&!r.hasCSS&&(r.hasCSS=!!n(e,"article,aside,figcaption,figure,footer,header,hgroup,nav,section{display:block}mark{background:#FF0;color:#000}")),y||c(e,r),e}var u=e.html5||{},s=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,f=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,d,p="_html5shiv",m=0,h={},y;!function(){try{var e=t.createElement("a");e.innerHTML="<xyz></xyz>",d="hidden"in e,y=1==e.childNodes.length||function(){t.createElement("a");var e=t.createDocumentFragment();return"undefined"==typeof e.cloneNode||"undefined"==typeof e.createDocumentFragment||"undefined"==typeof e.createElement}()}catch(n){d=!0,y=!0}}();var v={elements:u.elements||"abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video",shivCSS:u.shivCSS!==!1,supportsUnknownElements:y,shivMethods:u.shivMethods!==!1,type:"default",shivDocument:l,createElement:i,createDocumentFragment:a};e.html5=v,l(t)}(this,t),u._version=l,u._prefixes=v,u.mq=N,u.testStyles=w,f.className=f.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(s?" js "+S.join(" "):""),u}(this,this.document),function(e,t,n){function r(e){return"[object Function]"==m.call(e)}function o(e){return"string"==typeof e}function i(){}function a(e){return!e||"loaded"==e||"complete"==e||"uninitialized"==e}function c(){var e=h.shift();y=1,e?e.t?d(function(){("c"==e.t?T.injectCss:T.injectJs)(e.s,0,e.a,e.x,e.e,1)},0):(e(),c()):y=0}function l(e,n,r,o,i,l,u){function s(t){if(!m&&a(f.readyState)&&(E.r=m=1,!y&&c(),f.onload=f.onreadystatechange=null,t)){"img"!=e&&d(function(){b.removeChild(f)},50);for(var r in w[n])w[n].hasOwnProperty(r)&&w[n][r].onload()}}var u=u||T.errorTimeout,f=t.createElement(e),m=0,v=0,E={t:r,s:n,e:i,a:l,x:u};1===w[n]&&(v=1,w[n]=[]),"object"==e?f.data=n:(f.src=n,f.type=e),f.width=f.height="0",f.onerror=f.onload=f.onreadystatechange=function(){s.call(this,v)},h.splice(o,0,E),"img"!=e&&(v||2===w[n]?(b.insertBefore(f,g?null:p),d(s,u)):w[n].push(f))}function u(e,t,n,r,i){return y=0,t=t||"j",o(e)?l("c"==t?S:E,e,t,this.i++,n,r,i):(h.splice(this.i++,0,e),1==h.length&&c()),this}function s(){var e=T;return e.loader={load:u,i:0},e}var f=t.documentElement,d=e.setTimeout,p=t.getElementsByTagName("script")[0],m={}.toString,h=[],y=0,v="MozAppearance"in f.style,g=v&&!!t.createRange().compareNode,b=g?f:p.parentNode,f=e.opera&&"[object Opera]"==m.call(e.opera),f=!!t.attachEvent&&!f,E=v?"object":f?"script":"img",S=f?"script":E,C=Array.isArray||function(e){return"[object Array]"==m.call(e)},j=[],w={},N={timeout:function(e,t){return t.length&&(e.timeout=t[0]),e}},x,T;T=function(e){function t(e){var e=e.split("!"),t=j.length,n=e.pop(),r=e.length,n={url:n,origUrl:n,prefixes:e},o,i,a;for(i=0;r>i;i++)a=e[i].split("="),(o=N[a.shift()])&&(n=o(n,a));for(i=0;t>i;i++)n=j[i](n);return n}function a(e,o,i,a,c){var l=t(e),u=l.autoCallback;l.url.split(".").pop().split("?").shift(),l.bypass||(o&&(o=r(o)?o:o[e]||o[a]||o[e.split("/").pop().split("?")[0]]),l.instead?l.instead(e,o,i,a,c):(w[l.url]?l.noexec=!0:w[l.url]=1,i.load(l.url,l.forceCSS||!l.forceJS&&"css"==l.url.split(".").pop().split("?").shift()?"c":n,l.noexec,l.attrs,l.timeout),(r(o)||r(u))&&i.load(function(){s(),o&&o(l.origUrl,c,a),u&&u(l.origUrl,c,a),w[l.url]=2})))}function c(e,t){function n(e,n){if(e){if(o(e))n||(u=function(){var e=[].slice.call(arguments);s.apply(this,e),f()}),a(e,u,t,0,c);else if(Object(e)===e)for(p in d=function(){var t=0,n;for(n in e)e.hasOwnProperty(n)&&t++;return t}(),e)e.hasOwnProperty(p)&&(!n&&!--d&&(r(u)?u=function(){var e=[].slice.call(arguments);s.apply(this,e),f()}:u[p]=function(e){return function(){var t=[].slice.call(arguments);e&&e.apply(this,t),f()}}(s[p])),a(e[p],u,t,p,c))}else!n&&f()}var c=!!e.test,l=e.load||e.both,u=e.callback||i,s=u,f=e.complete||i,d,p;n(c?e.yep:e.nope,!!l),l&&n(l)}var l,u,f=this.yepnope.loader;if(o(e))a(e,0,f,0);else if(C(e))for(l=0;l<e.length;l++)u=e[l],o(u)?a(u,0,f,0):C(u)?T(u):Object(u)===u&&c(u,f);else Object(e)===e&&c(e,f)},T.addPrefix=function(e,t){N[e]=t},T.addFilter=function(e){j.push(e)},T.errorTimeout=1e4,null==t.readyState&&t.addEventListener&&(t.readyState="loading",t.addEventListener("DOMContentLoaded",x=function(){t.removeEventListener("DOMContentLoaded",x,0),t.readyState="complete"},0)),e.yepnope=s(),e.yepnope.executeStack=c,e.yepnope.injectJs=function(e,n,r,o,l,u){var s=t.createElement("script"),f,m,o=o||T.errorTimeout;s.src=e;for(m in r)s.setAttribute(m,r[m]);n=u?c:n||i,s.onreadystatechange=s.onload=function(){!f&&a(s.readyState)&&(f=1,n(),s.onload=s.onreadystatechange=null)},d(function(){f||(f=1,n(1))},o),l?s.onload():p.parentNode.insertBefore(s,p)},e.yepnope.injectCss=function(e,n,r,o,a,l){var o=t.createElement("link"),u,n=l?c:n||i;o.href=e,o.rel="stylesheet",o.type="text/css";for(u in r)o.setAttribute(u,r[u]);a||(p.parentNode.insertBefore(o,p),d(n,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};