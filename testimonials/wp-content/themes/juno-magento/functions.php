<?php
//Mage::getUrl , Mage::getDesign()->getSkinUrl, Mage::app()->getStores()
function mage_enabler($name = 'frontend') {
    $mage_php_url = ABSPATH . '../app/Mage.php';
    if ( !empty( $mage_php_url ) && file_exists( $mage_php_url ) && !is_dir( $mage_php_url )) {
	require_once ( $mage_php_url );
	umask(0);
	Mage::app("default");
	return Mage::getSingleton("core/session", array("name" => "frontend"));
    }
}
mage_enabler();

add_image_size('thumb_220_195', 220, 195, true );
add_image_size('thumb_700_407', 700, 407, true );
add_image_size('thumb_700', 700, 9999, true );
add_image_size('thumb_214_143', 214, 143, true );

add_theme_support( 'post-thumbnails' );

function juno_init_scripts() {
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', '' );
}
add_action( 'init', 'juno_init_scripts', 1 );

function custom_wp_get_archives($args = '') {
	global $wpdb, $wp_locale;

	$defaults = array(
		'type' => 'monthly', 'limit' => '',
		'format' => 'html', 'before' => '',
		'after' => '', 'show_post_count' => false,
		'echo' => 1,
		'startyear' => 0,
		'endyear' => date('Y')
	);

	$r = wp_parse_args( $args, $defaults );
	extract( $r, EXTR_SKIP );

	if ( '' == $type )
		$type = 'monthly';

	if ( '' != $limit ) {
		$limit = absint($limit);
		$limit = ' LIMIT '.$limit;
	}

	// this is what will separate dates on weekly archive links
	$archive_week_separator = '&#8211;';

	// over-ride general date format ? 0 = no: use the date format set in Options, 1 = yes: over-ride
	$archive_date_format_over_ride = 0;

	// options for daily archive (only if you over-ride the general date format)
	$archive_day_date_format = 'Y/m/d';

	// options for weekly archive (only if you over-ride the general date format)
	$archive_week_start_date_format = 'Y/m/d';
	$archive_week_end_date_format	= 'Y/m/d';

	if ( !$archive_date_format_over_ride ) {
		$archive_day_date_format = get_option('date_format');
		$archive_week_start_date_format = get_option('date_format');
		$archive_week_end_date_format = get_option('date_format');
	}

	//filters
	//$where = apply_filters( 'getarchives_where', "WHERE post_type = 'post' AND post_status = 'publish'", $r );
	$where = apply_filters( 'getarchives_where', "WHERE post_type = 'post' AND post_status = 'publish' AND YEAR(post_date) >= $startyear AND YEAR(post_date) <= $endyear", $r );
	$join = apply_filters( 'getarchives_join', '', $r );

	$output = '';

	if ( 'monthly' == $type ) {
		$query = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, count(ID) as posts FROM $wpdb->posts $join $where GROUP BY YEAR(post_date), MONTH(post_date) ORDER BY post_date DESC $limit";
		$key = md5($query);
		$cache = wp_cache_get( 'wp_get_archives' , 'general');
		if ( !isset( $cache[ $key ] ) ) {
			$arcresults = $wpdb->get_results($query);
			$cache[ $key ] = $arcresults;
			wp_cache_set( 'wp_get_archives', $cache, 'general' );
		} else {
			$arcresults = $cache[ $key ];
		}
		if ( $arcresults ) {
			$afterafter = $after;
			foreach ( (array) $arcresults as $arcresult ) {
				$url = get_month_link( $arcresult->year, $arcresult->month );
				/* translators: 1: month name, 2: 4-digit year */
				$text = sprintf(__('%1$s %2$d'), $wp_locale->get_month($arcresult->month), $arcresult->year);
				if ( $show_post_count )
					$before = '<span class="count">'.$arcresult->posts . '</span>';
				$output .= get_archives_link($url, $text, $format, $before, $after);
			}
		}
	} elseif ('yearly' == $type) {
		$query = "SELECT YEAR(post_date) AS `year`, count(ID) as posts FROM $wpdb->posts $join $where GROUP BY YEAR(post_date) ORDER BY post_date DESC $limit";
		$key = md5($query);
		$cache = wp_cache_get( 'wp_get_archives' , 'general');
		if ( !isset( $cache[ $key ] ) ) {
			$arcresults = $wpdb->get_results($query);
			$cache[ $key ] = $arcresults;
			wp_cache_set( 'wp_get_archives', $cache, 'general' );
		} else {
			$arcresults = $cache[ $key ];
		}
		if ($arcresults) {
			$afterafter = $after;
			foreach ( (array) $arcresults as $arcresult) {
				$url = get_year_link($arcresult->year);
				$text = sprintf('%d', $arcresult->year);
				if ($show_post_count)
					$before = '<span class="count">'.$arcresult->posts . '</span>';
				$output .= get_archives_link($url, $text, $format, $before, $after);
			}
		}
	}
	if ( $echo )
		echo $output;
	else
		return $output;
}


function home_news(){
    $news = new WP_Query('post_type=post&posts_per_page=2');
    $output = "";
    if($news->have_posts()){ $i = 0;
	while($news->have_posts()){ $news->the_post(); $i++;
	    $class = ($i % 2 == 0) ? 'class="last"' : "";
	    $output .= '
	    <li ' . $class . '>
		<a href="' . get_permalink() . '" class="image">' . get_the_post_thumbnail(get_the_ID(), 'thumb_214_143') . '</a>   
		<a href="' . get_permalink() . '" class="title">' . get_the_title() . '</a>    
		<p>' . wp_trim_words(get_the_content(), 11) . '</p>    
		<a href="' . get_permalink() . '" class="read-more">Read more</a>
	    </li>';
	}
	$output = '<ul>' . $output . '</ul>';
    }
    
    echo $output;
    die();
}
add_action('wp_ajax_home_news', 'home_news');
add_action('wp_ajax_nopriv_home_news', 'home_news');

function juno_remove_menus(){
  remove_menu_page( 'edit.php' );                   //Posts
  remove_menu_page( 'edit-comments.php' );          //Comments
}
add_action( 'admin_menu', 'juno_remove_menus' );

