    
    <?php
    $block = Mage::getSingleton('core/layout');
    $footer = $block -> createBlock('page/html_footer');
    $footer -> setTemplate('page/html/footer.phtml');

	$footer -> setChild('newsletter-signup-text', Mage::app()->getLayout()->createBlock('cms/block')->setBlockId('newsletter-signup-text'));
	$footer -> setChild('Showrooms', Mage::app()->getLayout()->createBlock('cms/block')->setBlockId('showrooms'));
	$footer -> setChild('FooterLinks', Mage::app()->getLayout()->createBlock('cms/block')->setBlockId('footer-links'));
	$footer -> setChild('SocialMedia', Mage::app()->getLayout()->createBlock('cms/block')->setBlockId('social-media'));
	
	$footerNewsletter = $block -> createBlock('newsletter/subscribe');
	$footerNewsletter -> setTemplate('newsletter/subscribe.phtml');
	$footer -> setChild('footer_newsletter', $footerNewsletter);
    
    echo $footer -> toHTML();
    ?>    
    
    </div>
</div>

<?php wp_footer(); ?>

</body>
</html>