            <div class="col-right">
                <div class="block">
                    <h3>Categories</h3>
                    <ul>
                        <?php $categories = get_categories();
                        foreach ($categories as $category) { ?>
                        <li><a href="<?php echo get_category_link($category->term_id) ?>"><?php echo $category->name ?></a></li>
                        <?php } ?>
                    </ul>
                </div>	
                <div class="block block-archives">
                        <h3>Archives</h3>
                        <ul>
                            <?php custom_wp_get_archives(array('show_post_count' => 1, 'type' => 'monthly', 'startyear' => date('Y'))) ?>
                        </ul>
                </div>	
                <div class="block block-tweet bottom">
                    <h3>Latest Tweets</h3>
                    <div class="tweet"></div>                       
                </div>	
        </div><!-- .col-right -->