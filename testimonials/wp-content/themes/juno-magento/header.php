<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url') ?>/css/fancybox/jquery.fancybox-1.3.4.css" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
 $block = Mage::getSingleton('core/layout');
 $head = $block -> createBlock('page/html_head');

 $head -> addCss('css/widgets.css');
 $head -> addCss('css/local-sass.css');
 $head -> addCss('css/local.css');
 $head -> addCss('css/jquery.bxslider.css');
 $head -> addCss('css/jquery.selectBoxIt.css');
 $head -> addCss('css/blueimp-gallery.min.css');

 $head -> addJs('prototype/prototype.js');
 $head -> addJs('lib/ccard.js');
 $head -> addJs('prototype/validation.js');
 $head -> addJs('scriptaculous/builder.js');
 $head -> addJs('scriptaculous/effects.js');
 $head -> addJs('scriptaculous/dragdrop.js');
 $head -> addJs('scriptaculous/controls.js');
 $head -> addJs('scriptaculous/slider.js');
 $head -> addJs('varien/js.js');
 $head -> addJs('varien/form.js');
 $head -> addJs('varien/menu.js');
 $head -> addJs('mage/translate.js');
 $head -> addJs('mage/cookies.js');
 $head -> addJs('juno/jquery/jquery-1.9.1.min.js');
 $head -> addJs('juno/jquery/jquery-migrate-1.2.1.js');
 $head -> addJs('juno/jquery/jquery-ui.custom.min.js');
 $head -> addJs('juno/noConflict.js');
 $head -> addJs('juno/jquery/plugins/jquery.hoverIntent.minified.js');
 $head -> addJs('juno/jquery/plugins/jquery.bxslider.min.js');
 $head -> addJs('juno/jquery/plugins/jquery.selectBoxIt.min.js');
 $head -> addJs('juno/jquery/plugins/blueimp-gallery.min.js');
 $head -> addJs('juno/custom.js');

echo $head -> getCssJsHtml();
?>

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/jquery.fancybox-1.3.4.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/jquery.tweet.js"></script>

<script type="text/javascript">
    jQuery(function($){
        $(".tweet").tweet({
          username: "LilyGardner",
          count: 3,         
          loading_text: "loading tweets..."
        });
    });
</script>

<?php if ($cookies = Mage::getSingleton('Mage_Page_Block_Js_Cookie')) :
$cookies_path = $cookies->getPath();
$cookies_path = preg_replace("/\/[^\/]+?$/", "", $cookies_path);
$cookies_domain = $cookies->getDomain();
?>
<script type="text/javascript">
//<![CDATA[
Mage.Cookies.path     = '<?php echo $cookies_path; ?>';
Mage.Cookies.domain   = '<?php echo $cookies_domain; ?>';
//]]>
</script>
<?php endif; ?>

<script type="text/javascript">//<![CDATA[
        var Translator = new Translate([]);
        //]]></script>
<script type="text/javascript" src="//use.typekit.net/auo4ggc.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<?php
    
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
	wp_head();
?>
</head>
<body <?php body_class('journal'); ?>>
    <div class="wrapper">
	<div class="page">
	<?php
	$header = $block -> createBlock('page/html_header');
	$header -> setTemplate('page/html/header.phtml');
	
	if ($topLinks = $block -> createBlock('core/template')) {
		$topLinks -> setTemplate('cms/toplinks.phtml');
		
		$topLinks -> setChild('header_phone', Mage::app()->getLayout()->createBlock('cms/block')->setBlockId('header_phone'));
		$topLinks -> setChild('hand_delivery', Mage::app()->getLayout()->createBlock('cms/block')->setBlockId('hand_delivery'));
		$topLinks -> setChild('renowned_guarantees', Mage::app()->getLayout()->createBlock('cms/block')->setBlockId('renowned_guarantees'));
		
		$headerCartSidebar = $block -> createBlock('checkout/cart_sidebar');
		$headerCartSidebar -> setTemplate('checkout/cart/sidebar.phtml');
		$topLinks -> setChild('header_cart_sidebar', $headerCartSidebar);
	}
	
	if ($topMenu = $block -> createBlock('page/html_topmenu')) {
		$topMenu -> setTemplate('page/html/topmenu.phtml');
	}
	
	$topSearch = $block -> createBlock('core/template');
	$topSearch -> setTemplate('catalogsearch/form.mini.phtml');
	
	$topCurrency = $block -> createBlock("directory/currency");
	$topCurrency -> setTemplate('directory/currency.phtml');
	
	$header -> setChild('top_links', $topLinks);	
	$header -> setChild('topSearch', $topSearch);	
	$header -> setChild('topMenu', $topMenu);
	$header -> setChild('topCurrency', $topCurrency);
	echo $header -> toHTML();
	?>