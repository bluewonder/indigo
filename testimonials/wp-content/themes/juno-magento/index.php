<?php
get_header(); ?>
    <div id="body">
	<div class="contents">		
	    <div class="breadcrumbs">
		<ul>
		    <li class="home">
			<a href="<?php echo Mage::getUrl(''); ?>" title="Go to Home Page">Home</a>
		    </li>
		    <li class="last">
			<span>Blog</span>
		    </li>
		</ul>
	    </div>
				
	    <div class="col-main">
		    <ul class="grid-post">
			<?php
			$latest3 = new WP_Query('post_type=post&posts_per_page=3'); $i = 0;
			while($latest3->have_posts()): $latest3->the_post(); $i++
			?>
			    <li class="<?php echo $i % 3 == 0 ? 'last' : '' ; ?>">
				<a href="<?php the_permalink(); ?>" class="image"><?php the_post_thumbnail('thumb_220_195') ?>				</a>
				<a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a>
				<p><?php echo wp_trim_words($post->post_content, 20); ?></p>
				<a href="<?php the_permalink(); ?>" class="read-more">+</a>
			    </li>
			<?php endwhile; ?>
		    </ul>
		    
		    <?php
		    $others = new WP_Query('post_type=post&posts_per_page=3&offset=3');
		    if($others->have_posts()):
		    ?>
		    
		    <div class="blog-list">
			<ul class="list-post">
			    <?php while($others->have_posts()): $others->the_post(); ?>
			    <li>
				<?php if(has_post_thumbnail()): ?>
				<a href="<?php the_permalink(); ?>" class="image"><?php the_post_thumbnail('thumb_700_407') ?></a>
				<?php endif; ?>
				<a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a>
				<p class="posted-on">Posted on <?php the_time('F d, Y') ?></p>
				<p><?php echo wp_trim_words($post->post_content, 65); ?></p>
				<a href="<?php the_permalink(); ?>" class="read-more">+ Read</a>
			    </li>
			    <?php endwhile; ?>
			</ul>
			
			<?php wp_pagenavi(array('query' => $others)) ?>
		    </div><!-- .blog-list -->
		    <?php endif; ?>
		    
	    </div><!-- .col-main -->
	    
	    <?php get_sidebar(); ?>
				
	    </div><!-- .contents -->
    </div><!-- #body -->

<?php get_footer(); ?>