<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to twentyeleven_comment() which is
 * located in the functions.php file.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>
    <?php if ( post_password_required() ) : 	
	return;
    endif;
    ?>

    <?php // You can start editing here -- including this comment! ?>
    <div class="comments-box">
	<div class="title-box">
	    <h4 class="left">Comments (<?php echo get_comments_number() ?>)</h4>
	    <a id="leave-comment-link" href="#comment-form" class="right">Leave a Comment</a>
	</div>
	<?php if ( have_comments() ) : ?>	   

	    <ul class="comments-list">
		<?php foreach ($comments as $comment) : ?>
		<li>
		    <?php if ($comment->comment_approved == '0') : ?>
		    <p>Your comment is awaiting moderation.</p>
		    <?php endif; ?>
		    <?php comment_text() ?>
		    
		    <span class="post-by">Posted by <?php comment_author() ?></span>
		    
		</li>
		<?php endforeach; ?>
	    </ul>

	<?php
	/* If there are no comments and comments are closed, let's leave a little note, shall we?
	 * But we don't want the note on pages or post types that do not support comments.
	 */
	elseif ( ! comments_open() && ! is_page() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="nocomments"><?php _e( 'Comments are closed.', 'twentyeleven' ); ?></p>
	<?php endif; ?>

	<?php //comment_form(); ?>
	<div style="display: none">
	    <div id="comment-form">
		<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post">
		    <h2 class="title">Post a comment</h2>
		    <div class="content">
			<ul class="form-list bottom">
			    <?php if ( is_user_logged_in() ) : ?>
			    <li>
				<p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out &raquo;</a></p>
				<label class="required">Comment <em>*</em> :</label>
				<div class="input-box">
				    <textarea name="comment" id="comment" title="Message" class="required required-entry input-text" cols="5" rows="3"></textarea>
				</div>
			    </li>
			    <?php else : ?>
			    
			    <li>
				<label class="required">Name <em>*</em> :</label>
				<div class="input-box">
				    <input name="author" title="" class="input-text required-entry" type="text" value="<?php echo esc_attr($comment_author); ?>" />
				</div>
			    </li>
			    <li>
				<label class="required">E-mail address <em>*</em> :</label>
				<div class="input-box">
				    <input name="email" title="" class="input-text required-entry" type="text" value="<?php echo esc_attr($comment_author_email); ?>"/>
				</div>
			    </li>
			    <li class="bottom">
				<label class="required">Comment <em>*</em> :</label>
				<div class="input-box">
				    <textarea name="comment" id="comment" title="Message" class="required required-entry input-text" cols="5" rows="3"></textarea>
				</div>
			    </li>
			    <?php endif; ?>
			</ul>	
			<div class="buttons-set">
			    <?php comment_id_fields(); ?>
			    <?php do_action('comment_form', $post->ID); ?>
			    <button class="button" title="Post" type="submit"><span><span>Post</span></span></button>
			    <p class="required">* Required fields</p>
			</div>
		    </div>
		</form>	
	    </div>
	</div>
	<script type="text/javascript">
	jQuery(document).ready(function() {
	    jQuery("a#leave-comment-link").fancybox({
		'padding' : 20,
		'margin' : 0,
		'centerOnScroll' : true,
		'overlayColor' : '#000',
		'overlayOpacity' : 0.8,
		'titleShow' : false
	    });
	});
	</script>
	
    </div>