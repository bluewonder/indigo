<?php /*
Example template
Author: mitcho (Michael Yoshitaka Erlewine)
*/
?>
<?php if (have_posts()):?>
<div class="blog-related">
    <h4>Related Posts</h4>
    <ul class="bottom grid-post">
	<?php while (have_posts()) : the_post(); ?>
	<li>
	    <a href="<?php the_permalink(); ?>" class="image"><?php the_post_thumbnail('thumb_220_195'); ?></a>
	    <a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a>
	    <p><?php echo wp_trim_words($post->post_content, 20); ?></p>
	    <a href="<?php the_permalink(); ?>" class="read-more">+</a>
	</li>
	<?php endwhile; ?>
    </ul>
</div>
<?php endif; ?>