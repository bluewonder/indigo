<?php
get_header(); the_post(); ?>

    <div id="body">
        <div class="contents">
                	
            <div class="breadcrumbs">
		<ul>
		    <li class="home">
			<a href="<?php echo Mage::getUrl(''); ?>" title="Go to Home Page">Home</a>
		    </li>
		    <li class="last">
			<span>Blog</span>
		    </li>
		</ul>
	    </div>
					
	    <div class="col-main">						
		<div class="blog-single">
                    <div class="entry">
                        <?php if(has_post_thumbnail()): ?>
                        <p class="image"><?php the_post_thumbnail('thumb_700') ?></p>
                        <?php endif; ?>
                        <h2 class="title"><?php the_title(); ?></h2>
                        <p class="posted-on">Posted on <?php the_time('F d, Y') ?></p>
                        <?php the_content() ?>
                    </div>
		    
                    <?php related_posts(); ?>				
                    
		    <?php comments_template( '', true ); ?>
                                                            
                </div><!-- .blog-single -->
                    
            </div><!-- .col-main -->
					
            <?php get_sidebar(); ?>
                    
        </div><!-- .contents -->
    </div><!-- #body -->

<?php get_footer(); ?>