<?php 
/*
Template Name: Testimonials
*/
ini_set('display_errors', 1);
get_header(); ?>
    <div class="main-container col1-layout testimonials-page">
		<div class="main">		
			<div class="col-main">
				<div class="cms-wrapper">
					<div class="cms-template">
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<div class="full-screen-banner">
								<?php if ( has_post_thumbnail( $post->ID ) ) :
									$banner_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
									$banner_image = $banner_image[0];
									?><img src="<?php echo $banner_image; ?>" alt="Testimonials" class="base_img" /><?php 
					 			endif; ?>
								<div class="text-wrapper" style="margin-top: -311px;">
									<div class="text">
										<h1><?php the_title(); ?></h1>
										<?php the_content(); ?>
									</div>
								</div>
							</div>
						<?php endwhile; endif; ?>
				
					    <ul class="testimonials">
						<?php $testimonials_query = 'post_type=testimonials&posts_per_page=4';
						if ($paged = get_query_var('page')) :
							$testimonials_query .= "&paged=$paged";
						endif;
						$testimonials = new WP_Query($testimonials_query); 
						$i = 0;
						while($testimonials ->have_posts()): $testimonials ->the_post(); $i++
						?>
						    <li>
								<h2>&ldquo;<?php the_title(); ?>&rdquo;</h2>
								<?php the_content() ?>
								<?php if ($items_bought = get_post_meta($post->ID, 'items_bought', true)) :
									echo apply_filters('the_content', $items_bought);
								endif;
								if ($name = get_post_meta($post->ID, 'name', true)) :
									echo '<h3>' . $name . '</h3>';
								endif; ?>
						    </li>
						<?php endwhile; ?>
					    </ul>
			
						<?php $big = 999999999; // need an unlikely integer
			
						$pagination_vars = array(
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, get_query_var('page') ),
							'total' => $testimonials->max_num_pages,
							'prev_text'    => __('Previous'),
							'next_text'    => __('Next')
						);
			
						if ( $pagination = paginate_links( $pagination_vars ) ) :
							?><div class="pagination">Page: <?php echo $pagination; ?></div><?php 
						endif;
			
						?>
						
					</div><!-- .cms-template -->
		    	</div><!-- .cms-wrapper -->
				
				<?php /* 
				<block type="core/template" name="shop_list" template="custom/shop-list.phtml"/>
				*/
				$block = Mage::getSingleton('core/layout');
				
				$shop_list = $block -> createBlock('core/template');
				$shop_list -> setTemplate('custom/shop-list.phtml');

				echo $shop_list -> toHTML();
				?>
				
		    </div><!-- .col-main -->
	    </div><!-- .main -->
    </div><!-- .main-container -->

<?php get_footer(); ?>