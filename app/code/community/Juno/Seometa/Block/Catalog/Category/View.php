<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dave
 * Date: 13/08/13
 * Time: 22:20
 * To change this template use File | Settings | File Templates.
 */ 
class Juno_Seometa_Block_Catalog_Category_View extends Mage_Catalog_Block_Category_View {

    protected function _prepareLayout()
    {
        if(!Mage::getStoreConfig('juno_seometa/general/enabled')) {
            return parent::_prepareLayout();
        }

        $helper = Mage::helper('juno_seometa');

        Mage_Core_Block_Template::_prepareLayout();

        $this->getLayout()->createBlock('catalog/breadcrumbs');

        if ($headBlock = $this->getLayout()->getBlock('head')) {
            /* @var $category Mage_Catalog_Model_Category */
            $category = $this->getCurrentCategory();

            $algorithm = $helper->inheritMetaTitle($category);
            $title = $helper->parseAlgorithm($algorithm);

            //Fetch meta description
            $algorithm = $helper->inheritMetaDescription($category);
            $description = $helper->parseAlgorithm($algorithm);

            //Set meta values
            if ($title) {
                $headBlock->setTitle($title);
            }
            if($description) {
                $headBlock->setDescription($description);
            }

            //Default from here down
            if ($keywords = $category->getMetaKeywords()) {
                $headBlock->setKeywords($keywords);
            }
            if ($this->helper('catalog/category')->canUseCanonicalTag()) {
                $headBlock->addLinkRel('canonical', $category->getUrl());
            }
            /*
            want to show rss feed in the url
            */
            if ($this->IsRssCatalogEnable() && $this->IsTopCategory()) {
                $title = $this->helper('rss')->__('%s RSS Feed',$this->getCurrentCategory()->getName());
                $headBlock->addItem('rss', $this->getRssLink(), 'title="'.$title.'"');
            }
        }

        return $this;
    }
}