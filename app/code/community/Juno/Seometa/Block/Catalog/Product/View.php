<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dave
 * Date: 14/08/13
 * Time: 00:43
 * To change this template use File | Settings | File Templates.
 */ 
class Juno_Seometa_Block_Catalog_Product_View extends Mage_Catalog_Block_Product_View {

    /**
     * Add meta information from product to head block
     *
     * @return Mage_Catalog_Block_Product_View
     */
    protected function _prepareLayout()
    {
        if(!Mage::getStoreConfig('juno_seometa/general/enabled')) {
            return parent::_prepareLayout();
        }

        $helper = Mage::helper('juno_seometa');

        $this->getLayout()->createBlock('catalog/breadcrumbs');

        $headBlock = $this->getLayout()->getBlock('head');

        if ($headBlock) {

            $product = $this->getProduct();

            //Fetch meta title
            $algorithm = $product->getMetaTitle();
            if(!$algorithm) {
                //When empty, check whether to use store algorithm as default, or the product title attribute
                $algorithm = Mage::getStoreConfig('juno_seometa/general/products_default_meta_title') ?
                    Mage::getStoreConfig('juno_seometa/general/default_title_algorithm')
                    : $product->getName();
            }

            $title = $helper->parseAlgorithm($algorithm);
            if ($title) {
                $headBlock->setTitle($title);
            }

            $keyword = $product->getMetaKeyword();
            $currentCategory = Mage::registry('current_category');
            if ($keyword) {
                $headBlock->setKeywords($keyword);
            } elseif($currentCategory) {
                $headBlock->setKeywords($product->getName());
            }

            //Fetch meta description
            $algorithm = $product->getMetaDescription();
            if(!$algorithm) {
                //When empty, check whether to use store algorithm as default, or the product description attribute
                $algorithm = Mage::getStoreConfig('juno_seometa/general/products_default_meta_description') ?
                    Mage::getStoreConfig('juno_seometa/general/default_description_algorithm')
                    : Mage::helper('core/string')->substr($product->getDescription(), 0, 255);
            }

            $description = $helper->parseAlgorithm($algorithm);
            if ($description) {
                $headBlock->setDescription( ($description) );
            }

            if ($this->helper('catalog/product')->canUseCanonicalTag()) {
                $params = array('_ignore_category'=>true);
                $headBlock->addLinkRel('canonical', $product->getUrlModel()->getUrl($product, $params));
            }
        }

        return Mage_Catalog_Block_Product_Abstract::_prepareLayout();
    }

}