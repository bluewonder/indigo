<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dave
 * Date: 13/08/13
 * Time: 22:16
 * To change this template use File | Settings | File Templates.
 */ 
class Juno_Seometa_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * Parse algorithm, replacing placeholders, and return meta string
     * @param $algorithm String containing optional placeholders for dynamic values
     * @return bool|string Parsed meta string
     */
    public function parseAlgorithm($algorithm)
    {
        $parts = preg_split("/(\\[.*?\\])/", $algorithm, null, PREG_SPLIT_DELIM_CAPTURE);
        foreach($parts as &$part) {
            if(strpos($part, '[') !== FALSE) {
                //Remove square brackets
                $part = str_replace(array('[',']'), '', $part);

                //Split on optional part
                $subParts = explode('|',$part);

                //Fail quietly
                if(count($subParts) > 2) {
                    Mage::log(sprintf('Too many sub parts found parsing %s in algorithm %s'), $subParts, $algorithm);
                    return FALSE;
                }

                //Replace placeholder
                $part = $this->_replace($subParts[0]);

                //If we cannot get a value for the placeholder, use the default
                if(!$part && count($subParts) == 2 && !empty($subParts[1])) {
                    $part = $subParts[1];
                }

                //Replace with empty string if still bad part
                if(!$part) {
                    $part = '';
                }
            }
        }
        //Remove any empty parts
        $parts = array_filter($parts);
        //Trim all parts to avoid double spaces
        $parts = array_map('trim', $parts);
        return implode(' ', $parts);
    }

    /**
     * Replace placeholder text with real data
     * @param $placeholder
     * @return bool|null|string
     */
    protected function _replace($placeholder)
    {
        //No match found
        $result = FALSE;
        switch($placeholder) {
            //Store name
            case '%s':
                $result = Mage::app()->getStore()->getName();
                break;
            //Website name
            case '%w':
                $result = Mage::app()->getWebsite()->getName();
                break;
            //Category name
            case '%c':
                if(Mage::registry('current_category')) {
                    $result = Mage::registry('current_category')->getName();
                }
                break;
            //Product name
            case '%p';
                if(Mage::registry('current_product')) {
                    $result = Mage::registry('current_product')->getName();
                }
                break;
        }
        return $result;
    }

    /**
     * Fetch meta title for category depending on inheritance settings
     * @param $category Mage_Catalog_Model_Category
     * @return mixed
     */
    public function inheritMetaTitle($category)
    {
        //If there is no manually set meta title, determine fallback
        if(!$category->getMetaTitle()) {
            //Check for instruction to inherit
            if($category->getInheritMetaTitle() || (
                    $category->getInheritMetaTitle() === null &&
                    Mage::getStoreConfig('juno_seometa/general/categories_inherit_metatitle') &&
                    $category->getId() != Mage::app()->getStore()->getRootCategoryId()
                )) {
                return $this->inheritMetaTitle($category->getParentCategory());
            } else {
                //Fallback to store default
                /* @todo Handle empty case as we shouldn't output nothing... */
                return Mage::getStoreConfig('juno_seometa/general/default_title_algorithm');
            }
        } else {
            return $category->getMetaTitle();
        }
    }

    /**
     * Fetch meta description for category depending on inheritance settings
     * @param $category Mage_Catalog_Model_Category
     * @return mixed
     */
    public function inheritMetaDescription($category)
    {
        //If there is no manually set meta title, determine fallback
        if(!$category->getMetaDescription()) {
            //Check for instruction to inherit
            if($category->getInheritMetaDescription() || (
                    $category->getInheritMetaDescription() === null &&
                    Mage::getStoreConfig('juno_seometa/general/categories_inherit_metadescription') &&
                    $category->getId() != Mage::app()->getStore()->getRootCategoryId()
                )) {
                return $this->inheritMetaDescription($category->getParentCategory());
            } else {
                //Fallback to store default
                /* @todo Handle empty case as we shouldn't output nothing... */
                return Mage::getStoreConfig('juno_seometa/general/default_description_algorithm');
            }
        } else {
            return $category->getMetaDescription();
        }
    }
}