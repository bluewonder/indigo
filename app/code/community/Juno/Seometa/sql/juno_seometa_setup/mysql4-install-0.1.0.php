<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dave
 * Date: 13/08/13
 * Time: 22:16
 * To change this template use File | Settings | File Templates.
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$attribute  = array(
    'type' => 'int',
    'label'=> 'Inherit Page Title',
    'input' => 'select',
    'source' => 'eav/entity_attribute_source_boolean',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => "",
    'group' => "General Information",
    'input_renderer'=> 'juno_seometa/adminhtml_catalog_category_helper_metatitle'
);
$installer->addAttribute('catalog_category', 'inherit_meta_title', $attribute);

$attribute  = array(
    'type' => 'int',
    'label'=> 'Inherit Meta Description',
    'input' => 'select',
    'source' => 'eav/entity_attribute_source_boolean',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => "",
    'group' => "General Information",
    'input_renderer'=> 'juno_seometa/adminhtml_catalog_category_helper_metatitle'
);
$installer->addAttribute('catalog_category', 'inherit_meta_description', $attribute);

$installer->endSetup();