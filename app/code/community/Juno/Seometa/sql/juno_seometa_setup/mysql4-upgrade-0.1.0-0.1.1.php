<?php
/**
 * Disabled until we have a canonical category for products extension
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$attributeCode = 'inherit_meta_title';
$attributeGroupName = 'Meta Information';
$attributeLabel = 'Inherit Meta Title from Category';

$objCatalogEavSetup = Mage::getResourceModel('catalog/eav_mysql4_setup', 'core_setup');
$attributeExists = $objCatalogEavSetup->getAttributeId(Mage_Catalog_Model_Product::ENTITY, $attributeCode);

//Meta Title Inheritance Flag
if ($attributeExists === false) {
    $objCatalogEavSetup->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode, array(
        'group' => $attributeGroupName,
        'sort_order' => 100,
        'type' => 'int',
        'backend' => '',
        'frontend' => '',
        'label' => $attributeLabel,
        'input' => 'select',
        'class' => '',
        'source' => 'eav/entity_attribute_source_boolean',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'default' => '0',
        'visible_on_front' => false,
        'unique' => false,
        'is_configurable' => false,
        'used_for_promo_rules' => false
    ));
}

$attributeCode = 'inherit_meta_description';
$attributeLabel = 'Inherit Meta Description from Category';

$attributeExists = $objCatalogEavSetup->getAttributeId(Mage_Catalog_Model_Product::ENTITY, $attributeCode);

//Meta Description Inheritance Flag
if ($attributeExists === false) {
    $objCatalogEavSetup->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode, array(
        'group' => $attributeGroupName,
        'sort_order' => 101,
        'type' => 'int',
        'backend' => '',
        'frontend' => '',
        'label' => $attributeLabel,
        'input' => 'select',
        'class' => '',
        'source' => 'eav/entity_attribute_source_boolean',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'default' => '0',
        'visible_on_front' => false,
        'unique' => false,
        'is_configurable' => false,
        'used_for_promo_rules' => false
    ));
}

$installer->endSetup();