<?php
 /**
  *
  *
  **/

class Juno_Connector_Helper_Data extends Mage_Core_Helper_Abstract
{
	 
	 public function safeString($string)
	 {
	 	$string = str_replace('&#160;', ' ', $string);
	 	$string = str_replace('~', '-', $string);
	 	$string = mysql_escape_string($string);
	 	return $string;
	 }
	 
	 public function safeOrderString($string)
	 {
	 	// http://stackoverflow.com/questions/4389297/replace-foreign-characters
	 	$string = strtr($string, 
						  "���???��?����???????��???�???��������������??�???�??",
						  "AAAAAACEEEEIIIINOOOOOOYSaaaaaaceeeeiiiinoooooouuuuyy"); 
		$string = str_replace(array("'"), '', $string);
		$string = trim($string);
		$string = preg_replace("/[^a-zA-Z0-9]\s/", "", $string);
	 	return $string;
	 }
	 public function safeOrderTelephone($string)
	 {
	 	// http://stackoverflow.com/questions/4389297/replace-foreign-characters
		$string = str_replace(array(" ","+"), '', $string);
		$string = trim($string);
		$string = preg_replace("/[^0-9]/", "", $string);
	 	return $string;
	 }
	 
	 public function safeInsert($string)
	 {
	 	//$string = iconv("UTF-8", "ISO-8859-1", $string);
	 	$string = str_replace('&#160;', ' ', $string);
	 	$swaps = array("�"=>"e",'~'=>'-',"'"=>"","\\"=>"");
	 	foreach($swaps as $before=>$after){
			$string = str_replace($before, $after, $string); 	
	 	}
	 	$string = mysql_escape_string($string);
	 	return $string;
	 }
	 
	/**
	 * Format the decimal numbers.
	 */
	public function decimalFormat($number, $decimal_places = 2)
	{
		return number_format($number, $decimal_places, '.', '');
	}

}