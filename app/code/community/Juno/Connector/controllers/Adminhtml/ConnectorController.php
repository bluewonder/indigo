<?php
/**
 *  Juno - Connector Module
 *  @author Andy Slater <andy@junowebdesign.com>
 *  @copyright Juno Media <support@junowebdesign.com>
 *Mage::getStoreConfig('connector/settings/passphrase')
 */

class Juno_Connector_Adminhtml_ConnectorController extends Mage_Adminhtml_Controller_Action
{

    public function pullproductsAction()
    {
        Mage::getSingleton('core/session')->addSuccess('JUNO BRIDGE : Product Update : Thank you, your data download request has been sent, you will receive a update shortly.');
        $this->_doRequest('products');
        $this->_redirect('*/dashboard/');
    }

    public function pullcategoriesAction()
    {
        Mage::getSingleton('core/session')->addSuccess('JUNO BRIDGE : Category Update : Thank you, your data download request has been sent, you will receive a update shortly.');
        $this->_doRequest('categories');
        $this->_redirect('*/dashboard/');
    }

    public function pullstockAction()
    {
        Mage::getSingleton('core/session')->addSuccess('JUNO BRIDGE : Stock Update : Thank you, your data download request has been sent, you will receive a update shortly.');
        $this->_doRequest('stock');
        $this->_redirect('*/dashboard/');
    }

    private function _doRequest($type)
    {
        $string = array('order_ref'=>Mage::getStoreConfig('connector/settings/ordernumber'), 'type'=>Mage::getStoreConfig('connector/settings/type'), 'auth_code'=> md5(Mage::getStoreConfig('connector/settings/passphrase')));
        $fields_string = json_encode(array('data'=>$string));

        //open connection
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, 'http://www.junobridge.com/bridge/pullrequest/'.$type.'/');
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_CUSTOMREQUEST, "POST");
        //curl_setopt($ch,CURLOPT_HEADER, true);
        curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type:application/json','Content-Length: ' . strlen($fields_string)));
        $result = curl_exec($ch);
        //echo '<pre>'; print_r($result); echo '</pre>'; exit();

        $this->_addAdminMessage($result, $type);
    }

    private function _addAdminMessage($message, $type)
    {
        $write = Mage::getModel('core/resource')->getConnection('core_write');

        $data = array('severity'        => 4,
                      'date_added'      => date('Y-m-d H:i:s'),
                       'title'          => 'Juno Bridge: '.$type.' Results',
                       'description'    => $message
                     );

        //$write->insert('adminnotification_inbox', $data);

        return true;
    }

}