<?php
 /**
  *
  *
  **/

class Juno_Connector_JoinController extends Mage_Core_Controller_Front_Action
{
	
	public function indexAction()
	{
		die('Installed.');
	}
	
	/**
	 * Stock Levels
	 */
	public function stockAction()
	{
		$jsonStr = file_get_contents("php://input"); //read the HTTP body.
		$json = json_decode($jsonStr);
		$data = (array)$json->data;
		
		if($data['auth_code'] != md5(Mage::getStoreConfig('connector/settings/passphrase'))){
			die('Access Denied.');
		}
		Mage::getModel('connector/data_product_stock')->startImport($data['data']);
	}
	
	public function productsAction()
	{
		$jsonStr = file_get_contents("php://input"); //read the HTTP body.
		$json = json_decode($jsonStr);
		$data = (array)$json->data;
		
		if($data['auth_code'] != md5(Mage::getStoreConfig('connector/settings/passphrase'))){
			die('Access Denied.');
		}
		Mage::getModel('connector/data_product_import')->startImport($data['data']);
	}
	
	public function categoriesAction()
	{
		$jsonStr = file_get_contents("php://input"); //read the HTTP body.
		$json = json_decode($jsonStr);
		$data = (array)$json->data;
		
		if($data['auth_code'] != md5(Mage::getStoreConfig('connector/settings/passphrase'))){
			die('Access Denied.');
		}
		Mage::getModel('connector/data_category_category')->runService($data['data']);
	}
	
	public function brandsAction()
	{
		$jsonStr = file_get_contents("php://input"); //read the HTTP body.
		$json = json_decode($jsonStr);
		$data = (array)$json->data;
		
		if($data['auth_code'] != md5(Mage::getStoreConfig('connector/settings/passphrase'))){
			die('Access Denied.');
		}
		Mage::getModel('connector/data_product_brands')->runService($data['data']);
	}
	
	public function testAction()
	{
		$write = Mage::getModel('core/resource')->getConnection('core_write');
		
		$collection = $write->fetchAll($write->select()->from('catalog_product_super_attribute', array('COUNT(*) as rows','product_id'))->group('product_id'));
		
//		echo '<pre>'; print_r($collection); echo '</pre>'; exit();
		
		foreach($collection as $row){
			if($row['rows'] > 1){
				echo $row['product_id'].',';
			}
		}
		exit();		
		echo '<pre>'; print_r($collection); echo '</pre>'; exit();
		
	
	//	Mage::getModel('connector/data_product_abstract')->_updateConfigurables();
	}

	
    public function cronAction()
	{
		Mage::getModel('connector/data_order_cron')->collectOrders();
	}
	
	
	public function orderAction()
	{
		Mage::getModel('connector/data_order_sync')->runService();
	}

}