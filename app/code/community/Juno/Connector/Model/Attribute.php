<?php
 /**
  *
  *
  **/

class Juno_Connector_Model_Attribute extends Juno_Connector_Model_Abstract
{

	/**
     * Create an attribute.
     */
    public function createAttribute($code, $label, $type = 'int', $input = 'select')
    {
    	if($code == ''){
	    	return false;
    	}
        $model = Mage::getModel('eav/entity_setup','core_setup');
        $data = array('type'			=> 'int',
					  'input'			=> 'select',
					  'label'			=> $label,
					  'global'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
					  'required'		=> 0,
					  'is_comparable'	=> 0,
					  'is_searchable'	=> 0,
					  'is_system'		=> 0,
					  'is_unique'		=> 0,
					  'source'			=> 'eav/entity_attribute_source_table',
					  'is_configurable'	=> 1,
					  'user_defined'	=> 1);
 
		$model->addAttribute('catalog_product',$code,$data);
		$return = $model->getAttribute('catalog_product',$code);
		return $return['attribute_id'];
    }
}