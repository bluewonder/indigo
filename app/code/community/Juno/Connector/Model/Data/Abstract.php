<?php
 /**
  *
  *
  **/

class Juno_Connector_Model_Data_Abstract extends Juno_Connector_Model_Abstract
{
	/**
	 * Create a comment only visible in the admin for the order.
	 */
	protected function _orderComment($message, $order_id)
	{
		if($message != '' && $order_id){
			$write = $this->_getWriteAdapter();
			$write->insert($this->_getTableName('sales_flat_order_status_history'),
						   array('parent_id'			=> $order_id,
						   		 'is_visible_on_front'	=> 0,
						   		 'comment'				=> $message,
						   		 'status'				=> 'processing',
						   		 'created_at'			=> date('Y-m-d H:i:s')));
		}
	}
	
	protected function _transmitData($data, $url)
	{	
		$string = array('dispatch_type'=>Mage::getStoreConfig('connector/settings/dispatchtype'),'order_ref'=>Mage::getStoreConfig('connector/settings/ordernumber'), 'type'=>'JunoMagento', 'auth_code'=> md5(Mage::getStoreConfig('connector/settings/passphrase')), 'data' => $data);
		$fields_string = json_encode(array('data'=>$string));
		
		//open connection
		$ch = curl_init();
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, 1);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_CUSTOMREQUEST, "POST");
		//curl_setopt($ch,CURLOPT_HEADER, true);
		curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type:application/json','Content-Length: ' . strlen($fields_string)));
		$result = curl_exec($ch);
		
		//echo '<pre>'; print_r(strlen($fields_string)); echo '</pre>'; exit();

		return $result;	
	}
	
}