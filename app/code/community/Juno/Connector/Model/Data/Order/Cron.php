<?php
/**
 *
 *
 **/

class Juno_Connector_Model_Data_Order_Cron extends Juno_Connector_Model_Data_Order_Sync
{

	public function collectOrders()
	{	
		$order_numbers = $this->_findOrders();
		foreach($order_numbers as $order_number){
			Mage::register('order_number', $order_number);
			echo 'Sending the following order number to T2T: '.Mage::registry('order_number').', ';
			$this->runService();
			// Mage::log($order_number, null, 'adam-ordernumber.log');
			echo ' sent.<br />';
			Mage::unregister('order_number');
		}
		return true;
	}
	
	protected function _findOrders()
	{
		$r = $this->_getReadAdapter();
		
		$orders = array();// removed limit 100 - changed from 10/02/2014 23:10
		$collection = $r->fetchAll($q=$r->select()->from(array('o'=>'sales_flat_order'), array('entity_id','increment_id'))->where('o.status IN (?)', array('processing','complete'))->where('o.connector_internal_id = ?', '')->where('o.created_at > ?', '2014-03-02 00:00:00')->order('increment_id DESC')->limit(50));
		foreach($collection as $result){
			$orders[$result['entity_id']] = $result['increment_id'];
		}
		// Mage::log($orders, null, 'adam_orders.log');
		// exit();		
		// echo '<pre>'; print_r(array($q->__toString(), $orders));exit();
		return $orders;
	}
}