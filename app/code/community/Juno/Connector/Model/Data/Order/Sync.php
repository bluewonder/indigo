<?php
 /**
  *
  *
  **/

class Juno_Connector_Model_Data_Order_Sync extends Juno_Connector_Model_Data_Abstract
{

	private $_order = false;

	/**
	 * Main Logic that triggers the chain.
	 */
	public function runService()
	{	
		$data = $this->_getData();
		$result = $this->_transmitData($data, 'http://www.junobridge.com/index.php/bridge/receiver/orders/');

        // -- Save a comment on the order, visable in the admin.
        if((!strstr($result, 'already exists on the system')) && (!strstr($result, 'Error')) && (!strstr($result, 'not found')))
        {
            $this->_orderComment('Order Transmitted by Juno Connector :: Return Order Reference = '.$result, $data['order']['order_id']);
        }
        else if((strstr($result, 'Error')) || (!strstr($result, 'Order has been dispatched')))
        {
            Mage::log(array($result, $data['order']), null, 'orderTransmitError.log');
        }
        else
        {
            Mage::log(array($result, $data['order']), null, 'orderTransmitError.log');
        }
	
		// -- Save the comnnector ID in the db so we know whats been sent.
        $this->_saveConnectorId($result, $data['order']['order_id']);

		return true;
	}
	
    /**
     * Save the external order number into the Magento DB.
     */
    protected function _saveConnectorId($result, $order_id)
    {
        if(strstr($result, 'Error')){
            return true;
        }
        if(strstr($result, 'dispatched')){
        	$external_id = explode('.', $result);
	        $write = $this->_getWriteAdapter();
	        try
	        {
	            $write->update($this->_getTableName('sales_flat_order'),array('connector_internal_id' => $external_id[1]),array('entity_id = '.$order_id));
	        } catch(Exception $e){
	            //die('Error: '.$e);
	            Mage::log($e, null, 'salesflatordererror.log');
	        }
	        try
	        {
	            $write->update($this->_getTableName('sales_flat_order_grid'),array('connector_internal_id' => $external_id[1]),array('entity_id = '.$order_id));
	        } catch(Exception $e){
	            //die('Error: '.$e);
	            Mage::log($e, null, 'salesflatordergriderror.log');
	        }
	        return true;
        }
        if(strstr($result, 'unable to allocate stock')){
        	$parts = explode(' ', $result);
        	$external_id = substr($parts[count($parts)-1], 3);
        	try
	        {
	            $write->update($this->_getTableName('sales_flat_order'),array('connector_internal_id' => $external_id),array('entity_id = '.$order_id));
	        } catch(Exception $e){
	            //die('Error: '.$e);
	            Mage::log($e, null, 'salesflatordererror.log');
	        }
	        try
	        {
	            $write->update($this->_getTableName('sales_flat_order_grid'),array('connector_internal_id' => $external_id),array('entity_id = '.$order_id));
	        } catch(Exception $e){
	            //die('Error: '.$e);
	            Mage::log($e, null, 'salesflatordergriderror.log');
	        }
        	return true;
        }
    }
	
	/**
	 * Get the order details into a nicely formatted array.
	 */
	private function _getData($observer)
	{
		$params = Mage::app()->getRequest()->getParams();
		if($params['ref']){
			$order = Mage::getModel('sales/order')->loadByIncrementId($params['ref']);
		} elseif(Mage::registry('order_number')){
			$order = Mage::getModel('sales/order')->loadByIncrementId(Mage::registry('order_number'));
		} else {
			if($checkout = Mage::getSingleton('checkout/type_onepage')->getCheckout()){
				if ($checkout->getLastOrderId()){
					$order = Mage::getModel('sales/order')->load($checkout->getLastOrderId());	
				}
			} else {
				$order = $observer->getEvent()->getOrder();
			}
		}
		return array('order'	=> $this->_getOrder($order->getData(),$type),
					 'items'	=> $this->_getItems($order),
					 'payment'	=> $this->_getPayment($order->getPayment()->getData()),
					 'customer'	=> $this->_getCustomer($order->getData()),
					 'shipping'	=> $this->_getAddress($order->getShippingAddressId(),$order,'shipping'),
					 'billing'	=> $this->_getAddress($order->getBillingAddressId(),$order,'billing')
					 );
	}
	
	/**
	 * Get the order details.
	 */
	private function _getOrder($order,$type)
	{
		return array('order_increment'			=> $order['increment_id'],
					 'status'					=> $order['status'],
					 'coupon_code'				=> $order['coupon_code'],
					 'shipping_description'		=> $order['shipping_description'],
					 'base_discount_amount'		=> $this->_decimalFormat($order['base_discount_amount']),
					 'base_grand_total'			=> $this->_decimalFormat($order['base_grand_total']),
					 'base_shipping_total'		=> $this->_decimalFormat(($order['base_shipping_amount']+$order['base_shipping_tax_amount'])),
					 'base_shipping_amount'		=> $this->_decimalFormat($order['base_shipping_amount']),
					 'base_shipping_tax_amount'	=> $this->_decimalFormat($order['base_shipping_tax_amount']),
					 'base_subtotal'			=> $this->_decimalFormat($order['base_subtotal']),
					 'base_tax_amount'			=> $this->_decimalFormat($order['base_tax_amount']),
					 'total_qty_ordered'		=> $this->_decimalFormat($order['total_qty_ordered'],0),
					 'billing_address_id' 		=> $order['billing_address_id'],
					 'shipping_address_id' 		=> $order['shipping_address_id'],
					 'weight' 					=> $order['weight'],
					 'base_currency_code' 		=> $order['base_currency_code'],
					 'customer_email' 			=> $order['customer_email'],
					 'customer_firstname' 		=> $order['customer_firstname'],
					 'customer_lastname' 		=> $order['customer_lastname'],
					 'customer_prefix' 			=> $order['customer_prefix'],
					 'remote_ip' 				=> $order['remote_ip'],
					 'shipping_method' 			=> $order['shipping_method'],
					 'created_at' 				=> $order['created_at'],
					 'remote_ip' 				=> $order['remote_ip'],
					 'order_id'					=> $order['entity_id'],
					 'sync_type'				=> $type);
	}
	
	/**
	 * Get the item details.
	 */
	private function _getItems($order)
	{
		foreach($order->getItemsCollection() as $item){
			if($item->getProductType() == 'simple'){
				$product = Mage::getModel('catalog/product')->load($item->getProductId());

				$discount_amount = ($item->getParentItemId() != '') ? $parent[$item->getParentItemId()]['base_discount_amount'] : $this->_decimalFormat($item->getBaseDiscountAmount());
				
				if($discount_amount==0){
					$tax_amount = ($item->getParentItemId() != '') ? $parent[$item->getParentItemId()]['base_tax_amount'] : $this->_decimalFormat($item->getBaseTaxAmount());
				} else {
					$percent = ($item->getParentItemId() != '') ? $parent[$item->getParentItemId()]['tax_percent'] : $item->getTaxPercent(); //$item->getTaxPercent()
					$tax_percent = 1+($percent/100);
					$price_paid = ($item->getParentItemId() != '') ? $parent[$item->getParentItemId()]['base_original_price'] : $this->_decimalFormat($item->getBaseOriginalPrice());
					$price_paid = $price_paid-$discount_amount;
					$tax_amount = number_format($price_paid-($price_paid/$tax_percent), 2, '.', '');
				}

				$data[] = array('connector_internal_id'	=> $product->getConnectorInternalId(),
								'sku'					=> $product->getSku(),
								'qty'					=> $this->_decimalFormat($item->getQtyOrdered(),0),
								'base_price'			=> ($item->getParentItemId() != '') ? $parent[$item->getParentItemId()]['base_price'] : $this->_decimalFormat($item->getBasePrice()),
								'base_original_price'	=> ($item->getParentItemId() != '') ? $parent[$item->getParentItemId()]['base_original_price'] : $this->_decimalFormat($item->getBaseOriginalPrice()),
								'base_discount_amount'	=> $discount_amount,
					 			'base_tax_line_amount'	=> $tax_amount,
					 			'base_tax_amount'		=> ($tax_amount/$item->getQtyOrdered()),
					 			'base_row_total'		=> ($item->getParentItemId() != '') ? $parent[$item->getParentItemId()]['base_row_total'] : $this->_decimalFormat($item->getBaseRowTotal()),
					 			);
			}
			if($item->getProductType() == 'configurable'){
				$parent[$item->getId()]['base_price'] = $this->_decimalFormat($item->getBasePrice());
				$parent[$item->getId()]['base_original_price'] = $this->_decimalFormat($item->getBaseOriginalPrice());
				$parent[$item->getId()]['base_discount_amount'] = $this->_decimalFormat($item->getBaseDiscountAmount());
				$parent[$item->getId()]['base_tax_amount'] = $this->_decimalFormat($item->getBaseTaxAmount());
				$parent[$item->getId()]['tax_percent'] = $item->getTaxPercent();
				$parent[$item->getId()]['base_row_total'] = $item->getBaseRowTotal();
			}
			if ($_SERVER['REMOTE_ADDR'] == "81.138.7.236") {
				//echo '<pre>'; print_r($item->getData()); echo '</pre>';// exit();
			}
		}
		return $data;
	}
	
	/**
	 * Get the payment details.
	 */
	private function _getPayment($payment)
	{
		return array('method'			=> $payment['method'],
					 'cc_expiry'		=> $payment['cc_exp_year'],
					 'cc_type'			=> $payment['cc_type'],
					 'cc_name'			=> $payment['cc_owner'],
					 'cc_last4' 		=> $payment['cc_last4']);
	}
	
	/**
	 * Get the customer details.
	 */
	private function _getCustomer($order)
	{
		$_helper = Mage::helper('connector');

		if(!$order['customer_id']){
			// -- Guest Checkout (use the guest account)
			$order['customer_id'] = 1;
		}
		$customer = Mage::getModel('customer/customer')->load($order['customer_id']);
		
		if(!$customer['connector_internal_id']){
			// -- Try to register the customer with cybertill.
			//Mage::getModel('cybertill/push_register')->registerCustomer($order['customer_id']);
			$customer = Mage::getModel('customer/customer')->load($order['customer_id']);
			
			// -- If there is still no CT id wow there must be something wrong with the customer registration data 
			// -- going to cybertill, therefore use the guest account
			if(!$customer['connector_internal_id']){
				$customer = Mage::getModel('customer/customer')->load(1);
			}
		}
		return array('connector_internal_id'=> $customer['connector_internal_id'],
					 'prefix' 				=> $customer['prefix'],
					 'firstname' 			=> $_helper->safeOrderString($customer['firstname']),
					 'lastname' 			=> $_helper->safeOrderString($customer['lastname']),
					 'password' 			=> 'magentouser',
					 'email' 				=> $customer['email'],
					 'created_at' 			=> $customer['created_at']);
	}
	
	/**
	 * Get the billing/shipping address details.
	 */
	private function _getAddress($id,$order,$type)
	{
		$_helper = Mage::helper('connector');
		$address = Mage::getModel('customer/address')->load($this->_convertQuoteAddressId($id));
		
		// -- make sure the address --
		$account_address = $address->getData();
		if($type == 'billing'){
			$order_address = $order->getBillingAddress()->getData();
			$address = $order->getBillingAddress();
		} else {
			$order_address = $order->getShippingAddress()->getData();
			$address = $order->getShippingAddress();
		}
		if($order_address['postcode'] != $account_address['postcode'] || $order_address['city'] != $account_address['city'] || $order_address['street'] != $account_address['street']){
			// -- address doesnt match --
			if($address->getConnectorInternalId() > 1){
				$connector_internal_id = $address->getConnectorInternalId();
			} else {
				//$connector_internal_id = Mage::getModel('cybertill/push_newaddress')->addAddress($address, true);//Juno_Cybertill_Model_Push_Newaddress
			}
			$address = $order_address;
			$street = explode("\n",$address['street']);
			return array('connector_internal_id'=> $connector_internal_id,
						 'prefix' 				=> $address['prefix'],
						 'firstname' 			=> $_helper->safeOrderString($address['firstname']),
						 'lastname' 			=> $_helper->safeOrderString($address['lastname']),
						 'street' 				=> $_helper->safeOrderString($street[0]),
						 'street_two'			=> $_helper->safeOrderString($street[1]),
						 'city' 				=> $_helper->safeOrderString($address['city']),
						 'region' 				=> $_helper->safeOrderString($address['region']),
						 'postcode' 			=> $_helper->safeOrderString($address['postcode']),
						 'country_id' 			=> $_helper->safeOrderString($address['country_id']),
						 'telephone' 			=> $_helper->safeOrderTelephone($address['telephone']),
						 'created_at' 			=> $address['created_at']);
		}
		if(count($address->getData())==0){
			if($type == 'billing'){
				$address = $order->getBillingAddress()->getData();
			} else {
				$address = $order->getShippingAddress()->getData();
			}
		}
		
		if($address['connector_internal_id'] > 1){
			$connector_internal_id = $address['connector_internal_id'];
		} else {
			$connector_internal_id = $this->_getCtAddressId($this->_getAttributeId('connector_internal_id',$this->_getEntityType('customer/address')), $address['customer_address_id']);
		}
		
		$street = explode("\n",$address['street']);
		return array('connector_internal_id'=> $connector_internal_id,
					 'prefix' 				=> $address['prefix'],
					 'firstname' 			=> $_helper->safeOrderString($address['firstname']),
					 'lastname' 			=> $_helper->safeOrderString($address['lastname']),
					 'street' 				=> $_helper->safeOrderString($street[0]),
					 'street_two'			=> $_helper->safeOrderString($street[1]),
					 'city' 				=> $_helper->safeOrderString($address['city']),
					 'region' 				=> $_helper->safeOrderString($address['region']),
					 'postcode' 			=> $_helper->safeOrderString($address['postcode']),
					 'country_id' 			=> $_helper->safeOrderString($address['country_id']),
					 'telephone' 			=> $_helper->safeOrderTelephone($address['telephone']),
					 'created_at' 			=> $address['created_at']);
	}
	
	private function _convertQuoteAddressId($id)
	{
		$read = $this->_getReadAdapter();
		return $read->fetchOne($read->select()->from($this->_getTableName('sales_flat_order_address'), 'customer_address_id')->where('entity_id = ?', $id)->limit(1));
	}
	
	/**
	 * Format the decimal numbers.
	 */
	private function _getCtAddressId($attribute_id, $entity_id)
	{
		if($entity_id){
			$read = $this->_getReadAdapter();
			$sql = "SELECT `value` FROM `customer_address_entity_varchar` WHERE `entity_id`=".$entity_id." AND `attribute_id`=".$attribute_id." LIMIT 1;";
			$result = $read->fetchRow($sql);
			if($result['value']){
				return $result['value'];
			}
		}
		return false;
	}
	
	/**
	 * Format the decimal numbers.
	 */
	private function _decimalFormat($number, $decimal_places = 2)
	{
		return number_format($number, $decimal_places, '.', '');
	}
	
}