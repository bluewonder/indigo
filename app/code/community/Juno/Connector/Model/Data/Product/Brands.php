<?php
 /**
  *
  *
  **/

class Juno_Connector_Model_Data_Product_Brands extends Juno_Connector_Model_Data_Product_Abstract
{

	/**
	 * Main trigger.
	 */
	public function runService($data)
	{	
		$i = 0;
		$attribute_id = $this->_getAttributeId('manufacturer', $this->_getEntityType('catalog/product'));
		$option_values = $this->_getOptionValues($attribute_id);
		$option_values_alt = array_flip($option_values);
		
		foreach($data as $id=>$brand){
			//echo $id.'<pre>'; print_r($brand); echo '</pre>'; //exit();
			if(!in_array($brand->name, $option_values)) {
				try
				{
					$value_id = $this->_addOptionValue($attribute_id, $brand->name);
				} catch(Exception $e){
					//die('Error: '.$e);
				}
			} else {
				$value_id = $option_values_alt[$brand->name];
			}
			// Get the product IDs
			if(count($brand->products)>0){
				foreach($brand->products as $connector_internal_id){
					if($parent_id = $this->getPidFromConnectorId($connector_internal_id)){
						$entity_ids = array();
						$entity_ids[] = $parent_id;
						if($related_ids = $this->_getRelatedEntityIds($entity_ids[0])){
							foreach($related_ids as $id){
								$entity_ids[] = $id;
							}
						}
						
//						echo $connector_internal_id.'<pre>'; print_r($entity_ids); echo '</pre>'; exit();
						
						foreach($entity_ids as $entity_id){
							try
							{
								$write->insert($this->_getTableForAttribute($attribute_id), array('attribute_id'=>$attribute_id,'entity_id'=>$entity_id,'store_id'=>0,'entity_type_id'=>$this->_getEntityType('catalog/product'),'value'=>$value_id));
//								echo 'Insert:<pre>'; print_r(array('attribute_id'=>$attribute_id,'entity_id'=>$entity_id,'store_id'=>0,'entity_type_id'=>$this->_getEntityType('catalog/product'),'value'=>$value_id)); echo '</pre>'; exit();
							} catch(Exception $e){
//								echo 'Update:<pre>'; print_r(array('attribute_id'=>$attribute_id,'entity_id'=>$entity_id,'store_id'=>0,'entity_type_id'=>$this->_getEntityType('catalog/product'),'value'=>$value_id)); echo '</pre>'; exit();
								$write->update($this->_getTableForAttribute($attribute_id), array('value'=>$value_id), array(0=>'entity_id = '.$entity_id,1=>'attribute_id = '.$attribute_id,2=>'store_id = 0'));
							}
						}
					}
				}
			}
//			echo count($brand->products).'<pre>'; print_r($brand->products); echo '</pre>'; //exit();
//			echo $value_id.'<pre>'; print_r($option_values_alt); echo '</pre>'; exit();
		}
//		die('Here');
		return true;
	}
	
	/**
	 *
	 */
	protected function _getRelatedEntityIds($parent_id)
	{
		$ids = array();
		$read = $this->_getReadAdapter();
		$q=$read->select()->from($this->_table('catalog_product_super_link'), 'product_id')->where('parent_id = ?',$parent_id);
		
		die('Here: '.$q);
		
		
		try
		{
			$collection = $read->fetchAll($q);
		} catch(Exception $e) {
			die('Error: '.$e);
		}
		
//		echo $q->__toString().'<pre>'; print_r($collection); echo '</pre>'; exit('asdasd'."\n\n\n");
		foreach($collection as $item){
			$ids[] = $item['product_id'];
		}
		return $ids;
	}
	
	/**
	 * Check to see if the product already exists in the db, based on SKU.
	 */
	public function getPidFromConnectorId($connector_internal_id)
	{	
		$read = $this->_getReadAdapter(); 
		// -- check the internal id
		$attribute_id = $this->_getAttributeId('connector_internal_id', $this->_getEntityType('catalog/product'));
		if($entity_id = $read->fetchOne(
					$read->select()->from(array('v'=>$this->_getTableForAttribute($attribute_id)), 'entity_id')
//								->joinleft(array('e'=>$this->_getTableName('catalog_product_entity')), 'v.entity_id = e.entity_id', 'entity_id')
								->where('v.value = ?',$connector_internal_id)
//								->where('e.type_id = ?', $type)
								->limit(1))){
			//echo '<pre>'; print_r($entity_id); echo '</pre>'; exit();
			return $entity_id;
		}
		return false;
	}

}