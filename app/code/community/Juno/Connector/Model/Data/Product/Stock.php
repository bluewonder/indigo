<?php
 /**
  *
  *
  **/

class Juno_Connector_Model_Data_Product_Stock extends Juno_Connector_Model_Data_Product_Abstract
{

	public function startImport($data)
	{
	
		$this->log($data, 'stock');
	
		foreach($data as $item){
			$item = (array)$item;
			if(!isset($item['entity_id'])){
				$entity_id = $this->_getEntityId($item['connector_internal_id']);
			} else {
				$entity_id = $item['entity_id'];
			}
			//die('Here: '.$entity_id);
			if($entity_id){
				$this->setStockLevels($entity_id, $item['qty']);
				echo 'Set: '.$entity_id.' = '.$item['qty'].' qty<br />';
			}
		}
		$this->_updateConfigurables();
		return true;
	}

	private function _getEntityId($value)
	{
		$read = $this->_getReadAdapter();

		$attribute_id = $this->_getAttributeId('connector_internal_id');		
		$table = $this->_getTableForAttribute($attribute_id);
		
		return $read->fetchOne($read->select()->from($table, 'entity_id')->where('attribute_id = ?', $attribute_id)->where('value = ?', $value)->limit(1));
	}
	
}