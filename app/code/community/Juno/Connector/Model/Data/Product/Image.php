<?php
 /**
  *
  *
  **/

class Juno_Connector_Model_Data_Product_Image extends Juno_Connector_Model_Data_Product_Abstract
{

	/**
	 * Import the image to the correct place on the server.
	 */
	public function moveLocalImage($image_name)
	{
		$path = Mage::getBaseDir('media');
		
		$media_source = DS.'import'.DS;
		$media_location = DS.'catalog'.DS.'product'.DS;
		
		$addon_path = substr($image_name,0,1).'/'.substr($image_name,1,1);
		if(!is_dir($path.$media_location.$addon_path)){
			try
			{
				mkdir($path.$media_location.$addon_path, 0777, true);
			} catch(Exception $e) {
				echo 'Can not make the directory: '.$path.$media_location.$addon_path.'<br />';
				echo $e.'<br />';
				exit();
			}
		}
		
		if(file_exists($path.$media_location.$addon_path.'/'.$image_name)) {
			return '/'.$addon_path.'/'.$image_name;
		} else {
			if(copy($path.$media_source.$image_name, $path.$media_location.$addon_path.'/'.$image_name)){
				return '/'.$addon_path.'/'.$image_name;
			} else {
				return null;
			}
		}
		return null; 
	}

	/**
	 * Move a remote image to the server and then import it to the correct place.
	 */
	public function moveRemoteImage($image_name)
	{
		if(strstr($image_name, 'http')){
            $remotePath = (strstr($image_name, 'images/product/')) ? 'images/product/':'images/productoption/'; // allow for item images
			$filename = explode($remotePath, $image_name);
			$count = (count($filename)-1);
			$filename = $filename[$count];
			$filename = str_replace('%20', ' ', $filename);
			$local_path = Mage::getBaseDir('media').DS.'import'.DS;
			if(!file_exists($local_path.$filename)){
				//echo 'Move file: '.$image_name.' -> '.$local_path.$filename.'<br />';
				copy($image_name, $local_path.$filename);
			}
		} else {
			$filename = $image_name;
		}
		return $this->moveLocalImage($filename);
	}
	
	/**
	 * Find images with that sku in.
	 */
	public function findImages($sku)
	{
		if(!$sku){
			return false;
		}
		$location = Mage::getBaseDir('media').DS.'import'.DS;
		//echo 'Find images with '.$location.$sku.' in the name.<br />'; 

		$clean_data = array();
		$files = glob($location.$sku."*.jpg");
		
		if(count($files) > 0) {
			//echo '<pre>'; print_r($files); echo '</pre>';// exit();
			$folder = 'import';
        	foreach($files as $file){
        		$file = explode($folder."/", $file);
				$clean_data[] = $this->moveLocalImage($file[1]);
			}
			//echo '<pre>'; print_r($clean_data); echo '</pre>'; exit();
		    return $clean_data;
		} else {
		    return false;
		}	
	}
	
}