	<?php
 /**
  *
  *
  **/

class Juno_Connector_Model_Data_Product_Import extends Juno_Connector_Model_Data_Product_Abstract
{

	/**
	 * Main trigger.
	 */
	public function startImport($data)
	{	
		//echo '<pre>'; print_r($data); echo '</pre>'; exit();
 		$i = 0;
		foreach($data as $product){
			$product = (array)$product;
			$product['items'] = (array)$product['items'];
			
			// echo '<pre>'; print_r($product); echo '</pre>'; exit();
			
			// -- Search locally for images.
			if($images = $this->_findLocalImages($product)){
//				echo '<pre>';print_r($images);exit();
				$product['gallery'] = $images;
				$image_attributes = array('image','small_image','thumbnail');
				foreach($image_attributes as $image_attribute){
					if($product[$image_attribute] == ''){
						$product[$image_attribute] = $images[0];
					}				
				}
			}
			// echo '<pre>'; print_r($product); echo '</pre>'; exit();
			$parent_id = null;
			// -- check if config item
			if(count($product['items'])>1){
				$parent_id = Mage::getModel('connector/data_product_configurable')->addUpdate($product);
				foreach($product['items'] as $item){
					//echo '<pre>'; print_r((array)$item); echo '</pre>'; exit();

                    $item['visibility'] = 1;
					$simple = Mage::getModel('connector/data_product_simple')->addUpdate((array)$item, $parent_id);
				}
                $this->checkCustomOptions($product, $parent_id);
			} else {
				$simple = Mage::getModel('connector/data_product_simple')->addUpdate($product);
			}
			$i++;
			if($i == 1){
				//die('Done.');
			}
		}
		$this->_updateConfigurables();

        if(Mage::getStoreConfig('connector/options/allowdisable') == true){
            $this->disableDiscontinued();
        }
		return true;
	}

    public function checkCustomOptions($product, $entity_id)
    {
        foreach($product['items'] as $item){
            if(is_array($item['resource_group'])){

               // print_r($item); exit();

                // -- found the custom options, add them to the product.
                foreach($item['resource_group'] as $group){
                    foreach($group as $group_name=>$options){
                        $this->_addCustomOption($group_name, $options, 'radio', $entity_id);
                    }
                }
            }
        }
    }

    public function _addCustomOption($option_name, $options, $option_type, $entity_id)
    {
        $write = $this->_getWriteAdapter();

        // catalog_product_option - product_id / type (radio) / is_require = 0  ===option_id
        $write->insert('catalog_product_option', array('product_id'=>$entity_id, 'type'=>$option_type, 'is_require'=>0));
        $option_id = $write->lastInsertId();

        // catalog_product_option_title - option_id / title
        $write->insert('catalog_product_option_title', array('option_id'=>$option_id, 'title'=>$option_name));

        foreach($options as $option){
            // catalog_product_option_type_value - option_id  ===option_type_id
            $write->insert('catalog_product_option_type_value', array('option_id'=>$option_id));
            $option_type_id = $write->lastInsertId();

            // catalog_product_option_type_title - option_type_id / title
            $write->insert('catalog_product_option_type_title', array('option_type_id'=>$option_type_id, 'title'=>$option['name']));

            // catalog_product_option_type_price - option_type_id / price / price_type (fixed)
            $write->insert('catalog_product_option_type_price', array('option_type_id'=>$option_type_id, 'price'=>$option['price'], 'price_type'=>'fixed'));
        }
        $write->update('catalog_product_entity', array('has_options'=>1), array('entity_id = '.$entity_id));

        //print_r(array($option_name, $options, $option_type, $entity_id)); exit();
    }

	/**
	 * Loop through the SKU codes and check if there are any images in the media/import folder.
	 */
	private function _findLocalImages($product)
	{
		$data = array();
		if(count($product['items'])>1){
			$data[] = Mage::getModel('connector/data_product_image')->findImages($product['sku']);
			foreach($product['items'] as $item){
				$item = (array)$item;
				$data[] = Mage::getModel('connector/data_product_image')->findImages($item['sku']);
			}
		} else {
			$data[] = Mage::getModel('connector/data_product_image')->findImages($product['sku']);
		}
		
		$images = array();
		foreach($data as $sku_images){
			foreach($sku_images as $sku_image){
				$images[] = $sku_image;
			}
		}
		//echo '<pre>'; print_r($images); echo '</pre>'; exit();
		if(count($images)==0){
			return false;
		}
		return $images;
	}

}