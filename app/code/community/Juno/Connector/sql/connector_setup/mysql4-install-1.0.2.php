<?php
  /**
//   *
//   *
//   **/

 $installer = $this;
 $installer->startSetup();

 $setup = new Mage_Eav_Model_Entity_Setup('core_setup');

 $setup->addAttribute('catalog_category', 'connector_internal_id', array(
     'group'         => 'General',
     'input'         => 'text',
     'type'          => 'varchar',
     'label'         => 'Connector Internal ID',
     'backend'       => '',
     'visible'       => 0,
     'required'      => 0,
     'user_defined' 	=> 0,
     'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL));

 $setup->addAttribute('customer', 'connector_internal_id', array(
 	'label'		=> 'Connector Internal ID',
 	'type'		=> 'varchar',
 	'input'		=> 'text',
 	'visible'	=> true,
 	'required'	=> false,
 	'position'	=> 1));

 $setup->addAttribute('catalog_product', 'connector_internal_id', array(
     'group'         => 'Connector Internal ID',
     'input'         => 'text',
     'type'          => 'varchar',
     'label'         => 'Connector Internal ID',
     'backend'       => '',
     'visible'       => 0,
     'required'      => 0,
     'user_defined' 	=> 0,
     'searchable' 	=> 0,
     'filterable' 	=> 0,
     'comparable'    			 => 0,
     'visible_on_front' 			 => 0,
     'visible_in_advanced_search' => 0,
     'is_html_allowed_on_front'   => 0,
     'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL));

 $setup->addAttribute('customer_address', 'connector_internal_id', array(
 	'label'		=> 'Connector Internal ID',
 	'type'		=> 'varchar',
 	'input'		=> 'text',
 	'visible'	=> false,
 	'required'	=> false,
 	'position'	=> 1));

 $installer->run("ALTER TABLE {$this->getTable('sales_flat_order')} ADD `connector_internal_id` VARCHAR(65) NOT NULL AFTER `entity_id`;");
 $installer->run("ALTER TABLE {$this->getTable('sales_flat_order_grid')} ADD `connector_internal_id` VARCHAR(65) NOT NULL AFTER `entity_id`;");
 $installer->run("ALTER TABLE {$this->getTable('sales_flat_order_address')} ADD `connector_internal_id` VARCHAR(65) NOT NULL AFTER `parent_id`;");

 $installer->installEntities();
 $installer->endSetup();