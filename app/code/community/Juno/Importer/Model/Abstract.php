<?php
/**
 *
 *
 */

class Juno_Importer_Model_Abstract
{

    public function openXlsx($filename)
    {
        $object = PHPExcel_IOFactory::createReader('Excel2007');
        try
        {
            $object = $object->load(Mage::getBaseDir('base').DS.'dataexchange'.DS.$filename);
        } catch (Exception $e) {
            die("Error loading file: ".$e->getMessage()."<br />\n");
        }
        return $object;
    }

    // IMPORTED FUNCTIONS FROM JUNO CONNECTOR - MODEL/ABSTRACT.PHP
    /**
     * Get the read connection
     */
    protected function _getReadAdapter()
    {
        return Mage::getSingleton('core/resource')->getConnection('core_read');
    }

    /**
     * Get the write connection
     */
    protected function _getWriteAdapter()
    {
        return Mage::getSingleton('core/resource')->getConnection('core_write');
    }

    /**
     * Add or update a static attribute value
     */
    public function addUpdateStaticValue($table, $column, $value, $entity_id)
    {
        $write = $this->_getWriteAdapter();
        $write->update(str_replace('_static', '', $table),
            array($column => $value),
            array(0 => 'entity_id = '.$entity_id));
        return true;
    }

    /**
     * Add or update a attribute value
     */
    public function addUpdateAttribute($attribute_code, $entity_type, $entity_id, $value, $log = "updateattribute")
    {
        $write = $this->_getWriteAdapter();
        $entity_type_id = $this->_getEntityType($entity_type);
        $attribute_id 	= $this->_getAttributeId($attribute_code, $entity_type_id);
        $frontend_type 	= $this->_getFrontendInput($attribute_id);
        $table 			= $this->_getTableForAttribute($attribute_id);
        $is_config_attribute = $this->_getIsConfigAttribute($attribute_id);

        if(strstr($table, '_static')){
            return $this->addUpdateStaticValue($table, $attribute_code, $value, $entity_id);
        }

        if($table && $value != ''){
            // -- get current value_id
            $value_id = $write->fetchOne($write->select()->from($table)->where('attribute_id = ?', $attribute_id)->where('entity_id = ?', $entity_id)->limit(1));

            // -- make sure that the value is the correct type
            //if((substr($table,-3) == 'int' && !is_numeric($value)) || (substr($table,-3) == 'int' && $is_config_attribute)){
            if(substr($table,-3) == 'int' && $is_config_attribute){
                //echo $attribute_code.' -> '.$is_config_attribute."\n";exit();
                $value = $this->_getOptionValue($attribute_id, $value);
            }
            if($frontend_type == 'multiselect' && $value != ''){
                $before = $value;
                if(is_array($value)){
                    $new_value = array();
                    foreach($value as $option){
                        if(substr($option, -1) == ','){
                            $option = trim(substr($option, 0, -1));
                        }
                        if($option != ''){
                            $new_value[] = $this->_getOptionValue($attribute_id, $option);
                        }
                    }
                    $value = implode(',', $new_value);
                } else {
                    if(substr($value, -1) == ','){
                        $value = trim(substr($value, 0, -1));
                    }
                    $value = $this->_getOptionValue($attribute_id, $value);

                }
                //print_r(array($before, $value, $attribute_code, $entity_type, $entity_id, $table, $frontend_type, $entity_type_id)); //exit();
            }
            $where = null;
            try
            {
                if($value_id){
                    $write->update($table,
                        array('value' => $value),
                        array('value_id = '.$value_id));
                } else {
                    $write->insert($table,
                        array('attribute_id'   => $attribute_id,
                            'entity_type_id' => $entity_type_id,
                            'entity_id'	  => $entity_id,
                            'value'		  => $value));
                }
//				if($attribute_code == 'status'){ echo '<pre>'; print_r($c); echo '</pre>'; exit(); }
            } catch(Exception $e) {
                echo "Error: \n"; print_r(array($before, $value, $attribute_code, $entity_type, $entity_id, $table, $frontend_type, $entity_type_id)); exit();
//				echo '<pre>'; print_r($e); exit();
            }
            if(in_array($attribute_code, array('image', 'small_image', 'gallery', 'thumbnail'))){
                $this->_addToGallery($value, $entity_id, $store_id);
            }
        }
        return true;
    }

    /**
     * Handle image gallery uploads
     */
    private function _addToGallery($value, $entity_id)
    {
        $write = $this->_getWriteAdapter();
        $attribute_id = $this->_getAttributeId('media_gallery');
        if(!is_array($value)){
            $value = array(0=>$value);
        }
        foreach($value as $val){
            $value_id = $write->fetchOne($write->select()->from($this->_getTableName('catalog_product_entity_media_gallery'), 'value_id')->where('attribute_id = ?', $attribute_id)->where('value = ?', $val)->where('entity_id = ?', $entity_id)->limit(1));
            if(!$value_id){
                $write->insert($this->_getTableName('catalog_product_entity_media_gallery'),
                    array('attribute_id'	=> $attribute_id,
                        'entity_id'	=> $entity_id,
                        'value'		=> $val));
                $value_id = $write->lastInsertId();
            }
            $result = $write->fetchOne($write->select()->from($this->_getTableName('catalog_product_entity_media_gallery_value'), 'value_id')->where('value_id = ?', $value_id)->limit(1));
            if(!$result){
                $write->insert($this->_getTableName('catalog_product_entity_media_gallery_value'),
                    array('value_id'	=> $value_id,
                        'store_id'	=> 0,
                        'label'	=> '',
                        'position'	=> 0,
                        'disabled'	=> 0));
            }
        }
        return true;
    }

    /**
     * Get the Frontend Input
     */
    protected function _getFrontendInput($attribute_id)
    {
        $read = $this->_getReadAdapter();
        return $read->fetchOne($read->select()->from($this->_getTableName('eav_attribute'), 'frontend_input')->where('attribute_id = ?', $attribute_id)->limit(1));
    }

    /**
     * Get if the attribute is a config one.
     */
    protected function _getIsConfigAttribute($attribute_id)
    {
        $read = $this->_getReadAdapter();
        //$is_config = $read->fetchOne($read->select()->from($this->_getTableName('catalog_eav_attribute'), 'is_configurable')->where('attribute_id = ?', $attribute_id)->limit(1));
        //$is_defined = $read->fetchOne($read->select()->from($this->_getTableName('eav_attribute'), 'is_user_defined')->where('attribute_id = ?', $attribute_id)->limit(1));
        //echo $attribute_id.' -> ('.$is_config.') + ('.$is_defined.')'."\n";
        if($result = $read->fetchOne($read->select()->from($this->_getTableName('eav_attribute'), 'attribute_id')->where('attribute_id = ?', $attribute_id)->where('is_user_defined = ?', 1)->where('source_model = ?', 'eav/entity_attribute_source_table')->limit(1))){
            //if(($is_config+$is_defined)==2){
            return true;
        }
        return false;
    }

    /**
     * Get the Backend Type
     */
    protected function _getBackendType($attribute_id)
    {
        $read = $this->_getReadAdapter();
        return $read->fetchOne($read->select()->from($this->_getTableName('eav_attribute'), 'backend_type')->where('attribute_id = ?', $attribute_id)->limit(1));
    }

    /**
     * Convert a group name to a group ID
     */
    private function _convertGroupToId($group)
    {
        $write = $this->_getWriteAdapter();
        $group_id = $write->fetchOne($write->select()->from($this->_getTableName('customer_group'), 'customer_group_id')->where('customer_group_code = ?', $group)->limit(1));
        if($group_id == ''){
            $group_id = 1;
        }
        return $group_id;
    }

    /**
     * Get the table name with relative prefix if needed.
     */
    protected function _getTableName($table)
    {
        return Mage::getSingleton('core/resource')->getTableName($table);
    }

    /**
     * Get the relative table for a attribute.
     */
    protected function _getTableForAttribute($attribute_id)
    {
        $read = $this->_getReadAdapter();
        $result = $read->fetchRow($read->select()->from(array('e'=>$this->_getTableName('eav_attribute')), array('backend_type'))
            ->joinleft(array('em'=>$this->_getTableName('eav_entity_type')), 'e.entity_type_id = em.entity_type_id', 'entity_model')
            ->where('e.attribute_id = ?', $attribute_id)
            ->limit(1));
        $entity_model = explode('/',$result['entity_model']);
        $entity_model = array_merge($entity_model, array('entity', $result['backend_type']));
        $entity_model = array_flip(array_flip($entity_model));
        $table = implode('_',$entity_model);

        if($table == '_entity'){
            return false;
        }
        return $this->_getTableName($table);
    }

    /**
     * Get the value_id of a attribute type dropdown
     */
    protected function _getOptionValue($attribute_id, $value)
    {
        $read = $this->_getReadAdapter();
        $option_id = $read->fetchOne($read->select()->from(array('eao'=>$this->_getTableName('eav_attribute_option')))
            ->joinleft(array('eaov'=>$this->_getTableName('eav_attribute_option_value')), 'eao.option_id = eaov.option_id', 'option_id')
            ->where('eao.attribute_id = ?', $attribute_id)
            ->where('eaov.value = ?', $value)
            ->limit(1));
        if(!$option_id){
            return $this->_addOptionValue($attribute_id, $value);
        }
        return $option_id;
    }

    /**
     * Add a option value
     */
    protected function _addOptionValue($attribute_id, $value)
    {
        $write = $this->_getWriteAdapter();
        $write->insert($this->_getTableName('eav_attribute_option'), array('attribute_id'=>$attribute_id));
        $write->insert($this->_getTableName('eav_attribute_option_value'), array('option_id'=>$write->lastInsertId(),'value'=>$value));
        return $write->fetchOne($write->select()->from($this->_getTableName('eav_attribute_option_value'), 'option_id')->where('value_id = ?', $write->lastInsertId())->limit(1));
    }

    /**
     * Get a attribute value
     */
    public function getAttributeValue($attribute_id, $entity_id)
    {
        $read = $this->_getReadAdapter();
        $frontend_type 	= $this->_getFrontendInput($attribute_id);
        $table 			= $this->_getTableForAttribute($attribute_id);

        $value = $read->fetchOne($read->select()->from($table, 'value')->where('entity_id = ?', $entity_id)->where('attribute_id = ?', $attribute_id)->limit(1));
        if($this->_getFrontendInput($attribute_id) == "select" || $this->_getFrontendInput($attribute_id) == "multiselect"){
            if(strstr($value, ',')){
                $value = $this->_findMultiOptionValue($value);
            } else {
                $value = $this->_findOptionValue($value);
            }
        }
        return $value;
    }

    /**
     * Find a the name of the option value
     */
    protected function _findOptionValue($value)
    {
        $read = $this->_getReadAdapter();
        return $read->fetchOne($read->select()->from($this->_getTableName('eav_attribute_option_value'), 'value')->where('value_id = ?', $value)->limit(1));
    }

    /**
     * Find a the name of multiple option values
     */
    protected function _findMultiOptionValue($value)
    {
        $values = explode(',',$value);
        $return = '';
        foreach($values as $val){
            $return .= $this->_findOptionValue($val).',';
        }
        return rtrim($return, ',');
    }

    /**
     * Get the Attribute ID
     */
    protected function _getAttributeId($attribute_code, $entity_type_id = 4)
    {
        $read = $this->_getReadAdapter();
        return $read->fetchOne($read->select()->from($this->_getTableName('eav_attribute'), 'attribute_id')->where('attribute_code = ?', $attribute_code)->where('entity_type_id = ?', $entity_type_id)->limit(1));
    }

    /**
     * Get the Entity Type ID
     */
    protected function _getEntityType($type)
    {
        $read = $this->_getReadAdapter();
        return $read->fetchOne($read->select()->from($this->_getTableName('eav_entity_type'), 'entity_type_id')->where('entity_model = ?', $type)->limit(1));
    }

    /**
     * Get the attribute set id.
     */
    public function getAttributeSetId($name, $entity_type_id = 4)
    {
        $read = $this->_getReadAdapter();
        if(!isset($this->_cache['attribute_set_id'][$entity_type_id][$name])){
            $this->_cache['attribute_set_id'][$entity_type_id][$name] = $read->fetchOne($read->select()->from($this->_getTableName('eav_attribute_set'), 'attribute_set_id')->where('attribute_set_name = ?', $name)->where('entity_type_id = ?', $entity_type_id)->limit(1));
        }
        return $this->_cache['attribute_set_id'][$entity_type_id][$name];
    }

    /**
     * Get the Product ID from the SKU
     */
    protected function getPidFromSku($sku)
    {
        $read = $this->_getReadAdapter();
        return $read->fetchOne($read->select()->from($this->_getTableName('catalog_product_entity'), 'entity_id')->where('sku = ?', $sku)->limit(1));
    }

    /**
     * Get the option values.
     */
    protected function _getOptionValues($attribute_id)
    {
        $read = $this->_getReadAdapter();
        $options = $read->fetchAll($read->select()->from(array('o' => $this->_getTableName('eav_attribute_option')), 'option_id')->join(array('v'=>$this->_getTableName('eav_attribute_option_value')), $read->quoteInto("v.option_id=o.option_id"),"value")->where('v.store_id = ?', 0)->where('o.attribute_id=?', $attribute_id));

        foreach($options as $option){
            $clean_options[$option['option_id']] = $option['value'];
        }
        return $clean_options;
    }

    /**
     * Convert any reference to any other reference.
     */
    protected function _identifierConvert($from, $to, $value)
    {
        $read = $this->_getReadAdapter();
        $table_from = $this->_getTableForAttribute($from);
        $table_to 	= $this->_getTableForAttribute($to);

        if($table_from != $table_to){
            return $read->fetchOne($read->select()->from(array('f'=>$table_from), '')
                ->joinleft(array('t'=>$table_to), 'f.entity_id = t.entity_id', $to)
                ->where('f.'.$from.' = ?', $value)
                ->where('t.attribute_id = ?', $this->_getAttributeId($to))
                ->limit(1));
        } else {
            return $read->fetchOne($read->select()->from($table_from, $to)->where($from.' = ?', $value)->limit(1));
        }
    }

    /**
     * Write data out to the log
     **/
    public function log($msg, $service = 'unknown')
    {
        Mage::log($msg, false, 'junoconnector-'.$service.'.log', true);
    }
}