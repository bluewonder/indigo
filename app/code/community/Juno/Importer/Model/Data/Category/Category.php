<?php
 /**
  *
  *
  **/

class Juno_Importer_Model_Data_Category_Category extends Juno_Importer_Model_Data_Abstract
{

    protected $category_ids = array();

    /**
     * Check if a category exists, if not, create it, return the path id array.
     */
    public function runService($categories)
    {
        $level = 2;
        $parent_id = $this->_baseParentId();

        //print_r($categories); die();

        foreach($categories as $category){
            $category = trim($category);

//            $connector_internal_id = $level.'_'.$parent_id.'_'.trim(strtolower(str_replace(' ', '_', $category)));
            if($entity_id = $this->_categoryExists($category, $parent_id, $level)){
                $this->category_ids[1][] = $this->_updateCategory($category, $entity_id);
            } else {
                if($category != '' && !is_null($category)) {
                    $this->category_ids[1][] = $this->_addCategory($category, $parent_id, $level, '1/'.$parent_id);
                }
            }
//            $parent_id = $entity_id;
//            $level++;
        }
        return $this->category_ids;
    }


    /**
     * Based on the text path from the array, calculate the parent id.
     */
    private function _calculatePath($parent_id)
    {
        $read = $this->_getReadAdapter();
        return $read->fetchOne($q=$read->select()->from($this->_getTableName('catalog_category_entity'), 'path')->where('entity_id = ?', $parent_id)->limit(1));
    }

    /**
     * Get the ID of the root category.
     */
    private function _baseParentId()
    {
        $read = $this->_getReadAdapter();
        return $read->fetchOne($read->select()->from($this->_getTableName('catalog_category_entity'), 'entity_id')->where('level = ?', 1)->limit(1));
    }

    /**
     * Update category.
     */
    private function _updateCategory($category, $entity_id)
    {
        $attributes = array('name'=>$category->name, 'description'=>$category->description);
        foreach($attributes as $attribute_code=>$attribute_value){
//            echo $attribute_code." -> ".'catalog/category'." -> ".$entity_id." -> (".$connector_internal_id.") -> ".$attribute_value."\n";
            if($attribute_value != ''){
                $this->addUpdateAttribute($attribute_code, 'catalog/category', $entity_id, $attribute_value);
            }
        }
        if(count($category->products)>0){
            foreach($category->products as $product_id){
                Mage::getModel('junoimport/data_category_product')->doInsert($entity_id,$product_id);
            }
        }
        echo 'Updated ('.$category->name.') '.count($category->products).' products.'."\n";
        return $entity_id;
    }

    /**
     * Add category.
     */
    private function _addCategory($category, $parent_id, $level, $path)
    {
        $read = $this->_getReadAdapter();

        $new_category = Mage::getModel('catalog/category');
        $new_category->setName($category);
        $new_category->setPath($path);
        $new_category->setLevel($level);
        $new_category->setIsActive(1);
        $new_category->setIsAnchor(1);
        try
        {
            //echo '<pre>'; print_r($new_category->getData()); echo '</pre>'; //exit();
            $new_category->save();
        } catch(Exception $e) {
            die('Error: '.$e);
        }
        if(count($category->products)>0){
            foreach($category->products as $product_id){
                Mage::getModel('junoimport/data_category_product')->doInsert($new_category->getId(),$product_id);
            }
        }
        echo 'Added ('.$category->name.') '.count($category->products).' products.'."\n";
        return $new_category->getId();
    }

    /**
     * Find out if the category exists.
     */
    private function _categoryExists($category_name, $parent_id, $level)
    {
        $read = $this->_getReadAdapter();

        // -- internal id not found, so check the name and the path.
        $entity_id = $read->fetchOne($q=$read->select()->from(array('v'=>$this->_getTableName('catalog_category_entity_varchar')), '')
            ->joinleft(array('e'=>$this->_getTableName('catalog_category_entity')), 'v.entity_id = e.entity_id', 'entity_id')
            ->where('e.level = ?', $level)
            ->where('e.parent_id = ?', $parent_id)
            ->where('v.attribute_id = ?', $this->_getAttributeId('name',$this->_getEntityType('catalog/category')))
            ->where('v.value = ?', $category_name)
            ->limit(1));
        if($entity_id){
            //die('Here2: '.$q->__toString().' '.$entity_id);
            return $entity_id;
        }
        //die('Here3: '.$entity_id);
        return false;
    }

}