<?php
 /**
  *
  *
  **/

class Juno_Importer_Model_Data_Category_Product extends Juno_Importer_Model_Data_Abstract
{

	/**
	 * Insert the product into the category.
	 */
	public function insertProduct($categories, $entity_id)
	{
		foreach($categories as $category_ids){	
			foreach($category_ids as $category_id){
				$this->doInsert($category_id, $entity_id);
			}
		}
		return true;
	}
	
	/**
	 * Do the insert.
	 */
	public function doInsert($category_id, $entity_id)
	{
		$write = $this->_getWriteAdapter();
		$query = $write->select()
					   	->from($this->_getTableName('catalog_category_product'))
					   	->where('category_id = ?', $category_id)
					   	->where('product_id = ?', $entity_id);
		$results = $write->fetchAll($query);
		if(count($results)==0){
			if($category_id && $entity_id){
				$write->insert($this->_getTableName('catalog_category_product'), array('category_id'=>$category_id,'product_id'=>$entity_id));
			}
		}
		return true;
	}
	
	
	private function _getEntityId($value)
	{
		$read = $this->_getReadAdapter();

		$attribute_id = $this->_getAttributeId('connector_internal_id');		
		$table = $this->_getTableForAttribute($attribute_id);
		
		return $read->fetchOne($read->select()->from($table, 'entity_id')->where('attribute_id = ?', $attribute_id)->where('value = ?', $value)->limit(1));
	}
	
}