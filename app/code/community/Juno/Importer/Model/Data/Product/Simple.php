<?php
 /**
  *
  *
  **/

class Juno_Importer_Model_Data_Product_Simple extends Juno_Importer_Model_Data_Product_Abstract
{
    /**
     * Wrapper
     */
    public function addUpdate($data, $parent_id = null)
    {
        if($entity_id = $this->productExists($data['sku'], $data['connector_internal_id'], 'simple', $data['name'])){
            // -- Update the existing simple product.
            echo 'Updated Simple Product ('.$entity_id.') -> '.$data['sku']."\n";
            $entity_id = $this->_updateSimpleProduct($data, $entity_id, $parent_id);
            //$this->log('Simple product updated for SKU :: '.$data['sku'],'product');
        } else {
            // -- Create a new simple product.
            $entity_id = $this->_createSimpleProduct($data, $parent_id);
            echo 'Created Simple Product ('.$entity_id.') -> '.$data['sku']."\n";
            //$this->log('Simple product created for SKU :: '.$data['sku'],'product');
        }

        return $entity_id;
    }

    /**
     * Update Product - The product already exists in Magento so update it.
     */
    private function _updateSimpleProduct($attributes, $entity_id, $parent_id)
    {
        try
        {
            $write = $this->_getWriteAdapter();
            //unset($attributes['name'],$attributes['description'],$attributes['price'],$attributes['weight'],$attributes['qty']);
            $attributes['visibility'] = ($parent_id == null) ? 4 : 1;
            $attributes = $this->replaceImageAttributes($attributes);
            $this->addUpdateAttributes($attributes,$entity_id);
            if($attributes['qty'] > 0){
                $this->setStockLevels($entity_id,$attributes['qty'],1);
            }
            if($parent_id){
                $this->_addSuperLinks($entity_id, $parent_id);
            }
            if($attributes['category'] != ''){
                $this->doCategories($attributes['category'], $entity_id);
            }
            $write->update($this->_getTableName('catalog_product_entity'), array('updated_at'=>date('Y-m-d H:i:s')), array(0=>'entity_id = '.$entity_id));
        } catch(Exception $e) {
            echo '<pre>'; print_r($e); echo '</pre>'; exit();
        }
        return $entity_id;
    }

    /**
     * Create Simple Pfor ($row = 1; $row <= $highestRow; $row++){roduct
     */
    private function _createSimpleProduct($data, $parent_id)
    {
        if(Mage::getModel('catalog/product')->loadByAttribute('sku', $data['sku']))
        {
            return;
        }
        $write = $this->_getWriteAdapter();
        try
        {
            $write->insert(
                $this->_getTableName('catalog_product_entity'),
                array(
                    'sku'				=> $data['sku'],
                    'entity_type_id'	=> $this->_getEntityType('catalog/product'),
                    'attribute_set_id'	=> $this->getAttributeSetId('Default', $this->_getEntityType('catalog/product')),
                    'type_id'			=> 'simple',
                    'has_options'		=> 0,
                    'required_options'	=> 0,
                    'created_at'		=> date('Y-m-d H:i:s'),
                    'updated_at'		=> date('Y-m-d H:i:s')
                )
            );
            $entity_id = $write->lastInsertId();
        } catch(Exception $e) {
            echo '<pre>'; print_r($e); echo '</pre>'; exit();
        }
        $attributes = array_merge(
            array(
                'msrp'			=> null,
                'status'		=> 1,
                'visibility' 	=> ($parent_entity_id == null) ? 4 : 1,
                'enable_googlecheckout' => 1,
                'tax_class_id'	=> 2,
                'is_imported'	=> 0,
                'meta_keyword'	=> null,
                'custom_layout_update' => null,
                'url_key'		=> str_replace(' ', '-', strtolower($data['name'])),
                'options_container' => 'container2'),
            $data);
        try
        {
            $attributes = $this->replaceImageAttributes($attributes);
            $this->addUpdateAttributes($attributes,$entity_id);
            $this->setStockLevels($entity_id,$data['qty'],1);
            if($parent_id){
                $this->_addSuperLinks($entity_id, $parent_id);
            }
            if($attributes['category'] != ''){
                $this->doCategories($attributes['category'], $entity_id);
            }
        } catch(Exception $e) {
            echo '<pre>'; print_r($e); echo '</pre>'; exit();
        }
        return $entity_id;
    }

    /**
     * Add the super link to relate the product to the parent configurable.
     */
    private function _addSuperLinks($entity_id, $parent_entity_id)
    {
        $write = $this->_getWriteAdapter();
        $result = $write->fetchRow($write->select()->from($this->_getTableName('catalog_product_super_link'))->where('product_id = ?', $entity_id)->where('parent_id = ?', $parent_entity_id)->limit(1));
        if(!$result['product_id']){
            $write->insert(
                $this->_getTableName('catalog_product_super_link'),
                array(
                    'product_id'	=> $entity_id,
                    'parent_id'		=> $parent_entity_id
                )
            );

        }

        $parent_price = $this->getAttributeValue($this->_getAttributeId('price'), $parent_entity_id);
        $child_price  = $this->getAttributeValue($this->_getAttributeId('price'), $entity_id);
        if($child_price>$parent_price){
            $difference = $child_price-$parent_price;
            $psai = $write->fetchOne($write->select()->from($this->_getTableName('catalog_product_super_attribute'), 'product_super_attribute_id')->where('attribute_id = ?', $this->_getAttributeId('config_size'))->where('product_id = ?', $parent_entity_id)->limit(1));

            $value_index = $this->_getOptionValue($this->_getAttributeId('config_size'), $this->getAttributeValue($this->_getAttributeId('config_size'), $entity_id));

            try
            {
                $write->insert('catalog_product_super_attribute_pricing', array('product_super_attribute_id'=>$psai, 'value_index'=>$value_index, 'pricing_value'=>$difference));
            } catch(Exception $e){
                // already exists.
            }
        }

        $result = $write->fetchRow($write->select()->from($this->_getTableName('catalog_product_relation'))->where('child_id = ?', $entity_id)->where('parent_id = ?', $parent_entity_id)->limit(1));
        if(!$result['child_id']){
            $write->insert(
                $this->_getTableName('catalog_product_relation'),
                array(
                    'child_id'	=> $entity_id,
                    'parent_id'	=> $parent_entity_id
                )
            );
        }
        return true;
    }
}