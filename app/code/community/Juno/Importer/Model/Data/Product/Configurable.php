<?php
 /**
  *
  *
  **/

class Juno_Importer_Model_Data_Product_Configurable extends Juno_Importer_Model_Data_Product_Abstract
{
	/**
	 * Wrapper
	 */
	public function addUpdate($data)
	{
		$product_id = null;
        if($entity_id = $this->productExists($data['sku'], $data['connector_internal_id'], 'configurable', $data['name'])){
			// -- Update the existing configurable product.
			$entity_id = $this->_updateConfigProduct($data, $entity_id);
			//$this->log('Configurable product updated for SKU :: '.$data['sku'],'product');
			echo ('Configurable product updated for ('.$entity_id.') SKU :: '.$data['sku']."\n");
		} else {
			// -- Create a new configurable product.
			$entity_id = $this->_createConfigProduct($data);
			//$this->log('Configurable product created for SKU :: '.$data['sku'],'product');
			echo ('Configurable product created for ('.$entity_id.') SKU :: '.$data['sku']."\n");
		}

		return $entity_id;
	}
	
	/**
	 * Update Product - The product already exists in Magento so update it.
	 */
	private function _updateConfigProduct($attributes, $entity_id)
	{
		$write = $this->_getWriteAdapter();
		try
		{
			//unset($attributes['name'],$attributes['description'],$attributes['status'],$attributes['price'],$attributes['weight'],$attributes['qty']);
			if(!isset($attributes['visibility'])){ $attributes['visibility'] = 4; }
			$attributes['status'] = 1;
			$attributes = $this->replaceImageAttributes($attributes);
			$this->addUpdateAttributes($attributes,$entity_id);
            if($attributes['qty'] > 0){
			    $this->setStockLevels($entity_id,1,1);
            }
			$this->_addConfigurableData($attributes['configurable_on'], $entity_id);
			if($attributes['category'] != ''){
				$this->doCategories($attributes['category'], $entity_id);
			}
			$write->update($this->_getTableName('catalog_product_entity'), array('updated_at'=>date('Y-m-d H:i:s')), array(0=>'entity_id = '.$entity_id));
		} catch(Exception $e) {
			echo '<pre>'; print_r($e); echo '</pre>'; exit();
		}
		return $entity_id;
	}
	
	/**
	 * Create Simple Product - Add the product data to the Magento database.
	 */
	private function _createConfigProduct($data)
	{
		$write = $this->_getWriteAdapter();
		try
		{
			$write->insert(
				$this->_getTableName('catalog_product_entity'),
				array(
					'sku'				=> $data['sku'],
					'entity_type_id'	=> $this->_getEntityType('catalog/product'),
					'attribute_set_id'	=> $this->getAttributeSetId('Default', $this->_getEntityType('catalog/product')),
					'type_id'			=> 'configurable',
					'has_options'		=> 0,
					'required_options'	=> 0,
					'created_at'		=> date('Y-m-d H:i:s'),
					'updated_at'		=> date('Y-m-d H:i:s')
				)
			);
			$entity_id = $write->lastInsertId();
		} catch(Exception $e) {
			echo '<pre>'; print_r($e); echo '</pre>'; exit();
		}
		$attributes = array_merge(
						array(
							'msrp'			=> null,
							'status'		=> 1,
							'visibility' 	=> ($parent_entity_id == null) ? 4 : 1,
							'enable_googlecheckout' => 1,
							'tax_class_id'	=> 2,
							'is_imported'	=> 0,
							'meta_keyword'	=> null,
							'custom_layout_update' => null,
							'url_key'		=> str_replace(' ', '-', strtolower($data['name'])),
							'options_container' => 'container2'),
						$data);
		try
		{	
			$attributes = $this->replaceImageAttributes($attributes);
			$this->addUpdateAttributes($attributes,$entity_id);					
			$this->setStockLevels($entity_id,1,1);
			$this->_addConfigurableData($data['configurable_on'], $entity_id);
			if($attributes['category'] != ''){
				$this->doCategories($attributes['category'], $entity_id);
			}
		} catch(Exception $e) {
			echo '<pre>'; print_r($e); echo '</pre>'; exit();
		}
		return $entity_id;
	}
	
	/**
	 * Set the configurable options for this item.
	 */
	private function _addConfigurableData($configurable_on, $entity_id)
	{
		$write = $this->_getWriteAdapter();
		
		if(strstr($configurable_on, ',')){
			$configurable_on = explode(',', $configurable_on);
		} else {
			$configurable_on = array(0=>$configurable_on);
		}
		foreach($configurable_on as $attribute_code){
			$attribute_id = $this->_getAttributeId($attribute_code);
			if(!$attribute_id){
				$attribute_id = Mage::getModel('junoimport/attribute')->createAttribute($attribute_code,ucwords(str_replace('_', ' ', $attribute_code)));
			}
			if($attribute_id){
				$result = $write->fetchOne($q=$write->select()->from($this->_getTableName('catalog_product_super_attribute'), 'product_id')->where('attribute_id = ?', $attribute_id)->where('product_id = ?', $entity_id)->limit(1));
				if(!$result){
					$write->insert(
						$this->_getTableName('catalog_product_super_attribute'),
						array(
							'product_id'	=> $entity_id,
							'attribute_id'	=> $attribute_id
						)
					);
					$write->insert(
						$this->_getTableName('catalog_product_super_attribute_label'),
						array(
							'product_super_attribute_id' => $write->lastInsertId(),
							'value'						 => ucwords(str_replace('_', ' ', $attribute_code))
						)
					);
				}
			}
		}
	}
}