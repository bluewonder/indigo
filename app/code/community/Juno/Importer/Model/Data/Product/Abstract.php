<?php
 /**
  *
  *
  **/

class Juno_Importer_Model_Data_Product_Abstract extends Juno_Importer_Model_Data_Abstract
{

    protected $category_ids = array();

    /*
     * Check to see if the product already exists in the db, based on SKU.
     */
    public function productExists($sku, $internal_id, $type = 'simple', $name)
    {
        $read = $this->_getReadAdapter();

        // -- sku
        if($entity_id = $read->fetchOne($read->select()->from($this->_getTableName('catalog_product_entity'), 'entity_id')->where('sku = ?',$sku)->where('type_id = ?', $type)->limit(1))){
            return $entity_id;
        }

        // -- name
        if($type == 'configurable'){
            /*
                        if($entity_id = $read->fetchOne(
                                    $read->select()->from(array('v'=>$this->_getTableName('catalog_product_entity_varchar')), 'entity_id')
                                                ->joinleft(array('e'=>$this->_getTableName('catalog_product_entity')), 'v.entity_id = e.entity_id', 'entity_id')
                                                ->where('v.value = ?',$name)
                                                ->where('e.type_id = ?', $type)
                                                ->limit(1))){
                            return $entity_id;
                        }
            */
        }
        //echo 'Cant find:<pre>'; print_r($internal_id); echo '</pre>'; exit();

        // -- check the internal id
        $attribute_id = $this->_getAttributeId('connector_internal_id');
        if($entity_id = $read->fetchOne(
            $read->select()->from(array('v'=>$this->_getTableForAttribute($attribute_id)), 'entity_id')
                ->joinleft(array('e'=>$this->_getTableName('catalog_product_entity')), 'v.entity_id = e.entity_id', 'entity_id')
                ->where('v.value = ?',$internal_id)
                ->where('e.type_id = ?', $type)
                ->limit(1))){
            return $entity_id;
        }
        return false;
    }

    /**
     * Set the stock levels.
     */
    public function setStockLevels($entity_id, $qty = 0, $stock_status = 1)
    {
        $write = $this->_getWriteAdapter();

        if($qty == 0){
            $stock_status = 0;
        }
        $result = $write->fetchRow($q=$write->select()->from($this->_getTableName('cataloginventory_stock_item'))->where('product_id = ?',$entity_id)->limit(1));
        if($qty == ''){ $qty = 0; }
        if($result['qty']){
            $update = array(0=>'product_id = '.$entity_id);
            $data = array('qty' => $qty, 'is_in_stock' => $stock_status);
        } else {
            $update = false;
            $data =  array('item_id' 					=> '',
                'product_id' 				=> $entity_id,
                'stock_id' 					=> 1,
                'qty' 						=> $qty,
                'min_qty' 					=> 0,
                'use_config_min_qty'		 	=> 1,
                'is_qty_decimal' 			=> 0,
                'backorders' 				=> 0,
                'use_config_backorders' 		=> 1,
                'min_sale_qty' 				=> 1,
                'use_config_min_sale_qty'	=> 1,
                'max_sale_qty' 				=> 0,
                'use_config_max_sale_qty' 	=> 1,
                'is_in_stock' 				=> $stock_status,
                'low_stock_date' 			=> '',
                'notify_stock_qty' 			=> '',
                'use_config_notify_stock_qty'=> 1,
                'manage_stock' 				=> 0,
                'use_config_manage_stock'  	=> 1,
                //'stock_status_changed_auto'=> 1,
                'use_config_qty_increments'  => 1,
                'qty_increments' 			=> 0,
                //'use_config_enable_qty_inc'=> 1,
                'enable_qty_increments'		=> 0);
        }
        if($update){
            $write->update($this->_getTableName('cataloginventory_stock_item'), $data, $update);
        } else {
            $write->insert($this->_getTableName('cataloginventory_stock_item'), $data);
        }

        $result = $write->fetchRow($write->select()->from($this->_getTableName('cataloginventory_stock_status'))->where('product_id = ?',$entity_id)->limit(1));
        if($result['qty']){
            $update = array(0=>'product_id = '.$entity_id);
            $data = array('qty' => $qty, 'stock_status' => $stock_status);
        } else {
            $update = null;
            $data =  array('product_id' => $entity_id, 'qty' => $qty, 'website_id' => 1, 'stock_id' => 1, 'stock_status' => $stock_status);
        }
        if(!$update){
            $write->insert($this->_getTableName('cataloginventory_stock_status'), $data);
        } else {
            $write->update($this->_getTableName('cataloginventory_stock_status'), $data, $update);
        }
        $result = $write->fetchRow($write->select()->from($this->_getTableName('catalog_product_website'))->where('product_id = ?',$entity_id)->limit(1));
        if(!$result['product_id']){
            $write->insert($this->_getTableName('catalog_product_website'), array('product_id' => $entity_id, 'website_id' => 1));
        }
        if(Mage::getStoreConfig('connector/options/disableoos') == true)
        {
            if($qty == 0 && $stock_status == 0){
                $write->update($this->_getTableName('catalog_product_entity_int'), array('value'=>2), array(0=>'entity_id = '.$entity_id, 1=>'attribute_id = '.$this->_getAttributeId('status')));
            } else {
                $write->update($this->_getTableName('catalog_product_entity_int'), array('value'=>1), array(0=>'entity_id = '.$entity_id, 1=>'attribute_id = '.$this->_getAttributeId('status')));
            }
        }
        $this->_clearCache($entity_id);
        return true;
    }

    /**
     * Clear any related product data from the cache.
     */
    public function _clearCache($entity_id)
    {

        return true;
    }

    /**
     * Loop through the attributes and add/update each.
     */
    public function addUpdateAttributes($attributes, $entity_id)
    {
        foreach($attributes as $attribute_name=>$value){
            $this->log('Product ('.$entity_id.') :: '.$attribute_name.' => '.$value,'product');
            $this->addUpdateAttribute($attribute_name, 'catalog/product', $entity_id, $value);
        }
        return true;
    }

    /**
     * Replace the image attributes.
     */
    public function replaceImageAttributes($attributes)
    {
        $image_attributes = array('image','small_image','thumbnail');
        foreach($image_attributes as $image_attribute){
            if($attributes[$image_attribute] != ''){
                if(strstr($attributes[$image_attribute], 'http')){
                    $attributes[$image_attribute] = Mage::getModel('junoimport/data_product_image')->moveRemoteImage($attributes[$image_attribute]);
                }
            }
        }
        if(isset($attributes['gallery'])){
            $i = 0;
            foreach($attributes['gallery'] as $image){
                if(strstr($image, 'http')){
                    $attributes['gallery'][$i] = Mage::getModel('junoimport/data_product_image')->moveRemoteImage($image);
                }
                $i++;
            }
        }
        return $attributes;
    }

    /**
     * Update the parent products.
     */
    public function _updateConfigurables()
    {
        $write = $this->_getWriteAdapter();
        $status_id = $this->_getAttributeId('status');
        $vis_id = $this->_getAttributeId('visibility');

        $results = $write->fetchAll($write->select()->from($this->_getTableName('catalog_product_entity'), 'entity_id')->where('type_id IN (?)', array('grouped','configurable')));

        foreach($results as $i=>$product){
            $results = $write->fetchAll($q=$write->select()->from($this->_getTableName('catalog_product_super_link'), 'product_id')->where('parent_id = ?', $product['entity_id']));

            if(count($results)>0){
                $related_items = array(); foreach($results as $item){ $related_items[] = $item['product_id']; }
                $stock_status = 0;

                $child_stock_qty = $write->fetchOne($write->select()->from($this->_getTableName('cataloginventory_stock_item'), 'SUM(qty) AS total')->where('product_id IN (?)', $related_items)->where('qty > ?',0));
                if($child_stock_qty>0){
                    $stock_status = 1;
                }
                $write->update($this->_getTableName('cataloginventory_stock_item'),array('is_in_stock'=>$stock_status),array(0=>'item_id = '.$product['entity_id']));

//                $write->update($this->_getTableName('catalog_product_entity_int'),array('value'=>(($stock_status == 0) ? 2 : 1)),array(0=>'attribute_id = '.$status_id,1=>'entity_id = '.$product['entity_id']));

                $write->update($this->_getTableName('catalog_product_entity_int'),array('value'=>1),array(0=>'entity_id IN ('.implode(',',$related_items).')',1=>'attribute_id = '.$vis_id));
            }
        }
        return true;
    }

    /**
     * Do the category assignment
     */
    public function doCategories($categories, $entity_id)
    {
        if($this->category_ids = Mage::getModel('junoimport/data_category_category')->runService($categories)){
//            $this->category_ids[][] = $category_id;
            Mage::getModel('junoimport/data_category_product')->insertProduct($this->category_ids, $entity_id);
        }
        return true;
    }

    /**
     * Disable discontinued products - (discontinued = products not updated for 3 days)
     */
    public function disableDiscontinued()
    {
        $read  = $this->_getReadAdapter();
        $write = $this->_getWriteAdapter();
        $status_id = $this->_getAttributeId('status');
        $days = Mage::getStoreConfig('connector/options/disabledafter');
        $days = (ctype_digit($days)) ? $days : 3; // make sure just just numeric digits

        // get deadline date
        $date = new DateTime('now');
        $date->sub(new DateInterval('P'.$days.'D')); // add x days
        $deadline = $date->format('Y-m-d H:i:s');

        if($entity_ids = $read->fetchPairs($read->select()->from($this->_getTableName('catalog_product_entity'), array('entity_id', 'updated_at'))->where('updated_at != ?', NULL)))
        {
            echo 'Checking for discontinued products ...';

            foreach ($entity_ids as $entity_id => $last_updated)
            {

                if($last_updated < $deadline) // if not updated in last 3 days
                {
                    try
                    {
                        if($write->update($this->_getTableName('catalog_product_entity_int'),array('value' => 2), array(0=>'attribute_id = '.$status_id, 1=>'entity_id = '.$entity_id)))
                        {
                            echo 'Disabled discontinued product ('.$entity_id.')'."\n";
                            $this->log('Disabled product ('.$entity_id, 'discontinued'); // log disabled products
                        }

                    } catch(Exception $e)  {
                        echo '<pre>'; print_r($e); echo '</pre>'; exit();
                    }
                }
            }
        }
        return true;
    }
}