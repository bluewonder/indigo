<?php
/**
 * Class Juno_Importer_Model_Products
 *
 * - Script to import the data files in to Magento.
 *
 */

class Juno_Importer_Model_Products extends Juno_Importer_Model_Abstract
{

    private $headers = array();
    private $custom_options = array();

    public function startImport()
    {
        $this->getCustomOptions();

        $excel2 = $this->openXlsx('Products.xlsx');

        $sheet = $excel2->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $products = array();

        for ($row = 1; $row <= $highestRow; $row++){
            if($row == 1){
                $this->setHeaders($sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE),$highestRow,$row);
            }
            if($row>1 && $row>$start){
                if(!$data = $this->_convertRow($sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE),$highestRow,$row,$products)){
                    continue;
                }

                if($data['display_name'] != ''){
                    $data['name'] = $data['display_name'];
                }

                if($data['resource_group'] != ''){
                    $data_group = $data['resource_group'];
                    $data['resource_group'] = array();

                    if(strstr($data_group, ', ')){
                        $groups = explode(', ', $data_group);
                        foreach($groups as $group){
                            $data['resource_group'][] = $this->custom_options[$group];
                        }
                    } else {
                        $data['resource_group'][] = $this->custom_options[$data_group];
                    }
                }
                //echo '<pre>'; print_r($data); exit();

                $products[$data['name']] = $this->mergeConfigData($data,$products[$data['name']]);
                $products[$data['name']]['items'][] = $data;

                //print_r($products); die();
            }
        }
        // print_r($products); die();

        Mage::getModel('junoimport/data_product_import')->startImport($products);
    }

    protected function mergeConfigData($new_data, $existing_data)
    {
        if(!$existing_data){
            return $new_data;
        }
        $cheapest_price = $new_data['price'];
        foreach($new_data as $k=>$v){
            if($v == ''){
                unset($new_data[$k]);
            }
        }

        foreach($existing_data['items'] as $item){
            if($cheapest_price>$item['price']){
                $cheapest_price = $item['price'];
            }
        }
        $new_data['price'] = $cheapest_price;

        //    echo $existing_data['main_category']."\n";

        if(strstr($existing_data['main_category'], ',')){
            $existing_data['category'] = explode(',',$existing_data['main_category']);
        } else {
            $existing_data['category'][] = $existing_data['main_category'];
        }

        if($new_data['display_name'] != ''){

            //die($new_data['display_name']);

            $new_data['name'] = $new_data['display_name'];
        }


        $merged_data = array_merge($existing_data, $new_data);

        //  print_r(array($new_data, $existing_data, $merged_data, $cheapest_price, $custom_options)); exit();

        return $merged_data;
    }

    protected function _convertRow($row,$max,$i,$parent = null)
    {
        $data = array();
        if(strstr($row[0][0], '/') || $row[0][0] == ''){
            return false;
        }
        foreach($row[0] as $k=>$v){
            if(isset($data['name'])){
                $parent = $parent[$data['name']]['items'][0];
            }
            if(!in_array($this->headers[$k], array('description', 'name', 'resource_group', 'short_description'))){
                if(strstr($v, "\n")){
                    $v = explode("\n", $v);
                }
                if($v == '' && $parent != null){
                    $v = $parent[$this->headers[$k]];
                }
            }
            $data[$this->headers[$k]] = $v;
        }
        $static_values = array('qty'=>100, 'tax_class_id'=>2, 'weight'=>0, 'status'=>1, 'configurable_on'=>'config_size');
        return array_merge($data, $static_values);
    }

    /**
     *
     */
    protected function setHeaders($row,$max,$i)
    {
        foreach($row[0] as $header){
            $this->headers[] = str_replace(array('no.', 'unit_price', 'item_sub-category'), array('sku', 'price', 'category'), str_replace(array(' ','-'), '_', strtolower(trim($header))));
        }
        //echo '<pre>'; print_r($this->headers); exit();
        //$this->headers =
    }

    public function getCustomOptions()
    {
        $excel2 = $this->openXlsx('OptionGroups.xlsx');

        $sheet = $excel2->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row = 1; $row <= $highestRow; $row++){
            if($row>2){
                $line = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $this->custom_options[$line[0][1]][$line[0][1]][] = array('name'=>$line[0][3], 'price'=>$line[0][5]);
            }
        }
    }

}