<?php

$installer = $this;
$installer->startSetup();

$installer->run("CREATE TABLE `profile` (
  `profile_id` smallint(6) NOT NULL auto_increment,
  `category_id` int(11) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_position` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `picturetwo` varchar(250) NOT NULL,
  `thumbnailtwo` varchar(250) NOT NULL,
  `file` varchar(255) NOT NULL,
  `content_heading` varchar(255) NOT NULL default '',
  `content` mediumtext,
  `date` varchar(50) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `creation_time` datetime default NULL,
  `update_time` datetime default NULL,
  `is_active` tinyint(1) NOT NULL default '1',
  `sort_order` varchar(50) NOT NULL,
  PRIMARY KEY  (`profile_id`),
  KEY `identifier` (`content_heading`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8");

$installer->endSetup();

$installer->run("CREATE TABLE `profile_store` (
  `profile_id` smallint(6) NOT NULL,
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY  (`profile_id`,`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS Profiles to Stores';");

$installer->endSetup();

