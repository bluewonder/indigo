<?php

class RichardMason_Profile_Block_View extends Mage_Core_Block_Template {

	private $press_id = null;

	

	public function __construct() {

		parent::__construct();

		$query_var = $this->getRequest()->getParams();

		if($query_var['id']) 

			$this->setPressId($query_var['id']);			

	}

	

	function setPressId($id) {

		$this->press_id = $id;

	}

	

	function getPressId(){

		return $this->press_id;

	}

	

	function getContent() {

		$model = new RichardMason_Profile_Model_Profile();	

		$content = $model->load($this->getPressId());

		return $content;	

	}



}

?>