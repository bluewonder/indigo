<?php

class RichardMason_Profile_Block_Collection extends Mage_Core_Block_Template {

	private $collection = null;



	public function __construct() {

		parent::__construct();

		$collection = new RichardMason_Profile_Model_Profile();

		$collection = $collection->getCollection();

		$collection->setOrder('date', 'desc');

		

		$this->setCollection($collection);

	}

	function setCollection($collection) {

		$this->collection = $collection; 

	}

	function getCollection() {

		return $this->collection;

	}

	protected function _prepareLayout() {

		parent::_prepareLayout();

		$pager = $this -> getLayout() -> createBlock('page/html_pager', 'custom.pager');

		$pager->setTemplate('profile/pager.phtml');

		$pager->setLimit(1);

		$pager -> setCollection($this -> getCollection());

		$this -> setChild('pager', $pager);

		$this -> getCollection() -> load();

		return $this;

	}



	public function getPagerHtml() {

		return $this -> getChildHtml('pager');

	}



}



?>