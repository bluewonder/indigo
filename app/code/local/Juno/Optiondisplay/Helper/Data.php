<?php
/**
 * Created by PhpStorm.
 * User: quyendam
 * Date: 9/20/14
 * Time: 4:05 PM
 */ 
class Juno_Optiondisplay_Helper_Data extends Mage_Core_Helper_Abstract {

    private function getCustomPrefix()
    {
        $result = array();
        array_push($result, Mage::getStoreConfig('juno_optiondisplay/general/tab_one_prefix',
            Mage::app()->getStore()));
        array_push($result, Mage::getStoreConfig('juno_optiondisplay/general/tab_two_prefix',
            Mage::app()->getStore()));
        array_push($result, Mage::getStoreConfig('juno_optiondisplay/general/tab_three_prefix',
            Mage::app()->getStore()));
        return $result;
    }

    public function getCustomOptions($_productId, $position)
    {
        $result = array();

        if (in_array($position, array('one', 'two', 'three'))) {
            $path = "juno_optiondisplay/general/tab_".$position."_prefix";
            $_product = Mage::getSingleton('catalog/product')->load($_productId);
            $prefix = Mage::getStoreConfig($path, Mage::app()->getStore());
            $options = $_product->getOptions();
            foreach($options as $option) {
                $optionPrefix = explode("|", $option->getTitle());
                $optionPrefix = $optionPrefix[0];
                if (in_array($optionPrefix, $this->getCustomPrefix())) {
                    if ($optionPrefix==$prefix) {
                        array_push($result, $option);
                    }
                }
            }
        }

        return $result;
    }

    public function getCustomOptionGroups($_productId, $position)
    {
        $customOptions = $this->getCustomOptions($_productId, $position);
        $result = array();
        foreach ($customOptions as $option) {
            foreach ($option->getValues() as $value) {
                $prefix = explode(" - ", $value->getTitle());
                if (isset($prefix[0])) {
                    $prefix = $prefix[0];
                    if (!in_array($prefix, $result)) {
                        array_push($result, $prefix);
                    }
                }
            }
        }
        return $result;
    }

    public function getAllowAttributes($product)
    {
        return $product->getTypeInstance(true)
            ->getConfigurableAttributes($product);
    }
}