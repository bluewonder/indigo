<?php
$installer = $this;
$installer->startSetup();
 
$inputfilter = '';
$installer->updateAttribute('customer', 'firstname', 'is_required', '0' );
$installer->updateAttribute('customer', 'lastname', 'is_required', '0' );
$attribute = $installer->getAttribute('customer', 'lastname');
$attribute = $installer->getAttribute('customer', 'firstname');
$attributeBind = array(
    'input_filter' => $inputfilter,
);

$attributeWhere = $installer->getConnection()->quoteInto('attribute_id=?', $attribute['attribute_id']);
$installer->getConnection()->update($installer->getTable('customer/eav_attribute'), $attributeBind, $attributeWhere);
?>