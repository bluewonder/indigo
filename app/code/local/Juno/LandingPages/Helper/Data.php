<?php
/**
 * Juno_LandingPages Module
 *
 * - Module to extend the Mana module, enabling descriptions and images on the landing pages.
 */

class Juno_LandingPages_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Get the landing page titles and descriptions where available.
     */
    public function getLandingData()
    {
        $params = Mage::app()->getRequest()->getParams();
        if(isset($params['id'])){
            $category_id = $params['id'];
            unset($params['id']);
        }
       $config_category = Mage::getStoreConfig('mana/seo/seo_category');
       if($config_category != $category_id && $config_category > 0){
           return false;
       }

        if(count($params)==1){
            $read = Mage::getModel('core/resource')->getConnection('core_read');
            foreach($params as $k=>$v){

                $result = $read->fetchRow($read->select()->from('m_seo_url', array('landing_page_seo_description as description', 'landing_page_seo_image as image', 'landing_page_seo_title as title'))->where('option_id = ?', $v)->where('internal_name = ?',$k)->order('landing_page_seo_title DESC')->limit(1));

                if($result['title'].$result['description'].$result['image'] == ''){
                    return array('title' => $read->fetchOne($read->select()->from('eav_attribute_option_value', 'value')->where('option_id = ?', $v)->limit(1)));
                }
                return $result;
            }
        }
        if(count($params)>1){
            $read = Mage::getModel('core/resource')->getConnection('core_read');
            $title = '';
            foreach($params as $k=>$v){

                $title .= ' '.$read->fetchOne($read->select()->from('eav_attribute_option_value', 'value')->where('option_id = ?', $v)->limit(1));

            }
            return array('title' => $title);
        }
        return false;
    }

    /**
     * Check for a optimised landing page URL for attributes.
     */
    public function getLandingPageUrl($option_id, $attribute_code, $type = 'full', $seperator = '-', $prefix = '')
    {
        $read = Mage::getModel('core/resource')->getConnection('core_read');

        if($result = $read->fetchRow($read->select()->from('m_seo_url')->where('option_id = ?', $option_id)->where('internal_name = ?', $attribute_code)->order('id ASC')->limit(1))){
            if($type == 'full'){
                $config_category = Mage::getStoreConfig('mana/seo/seo_category');
                $category = Mage::getModel('catalog/category')->load($config_category);
                return $category->getUrl().$seperator.$result['final_url_key'];
            } elseif($type == 'prefixed'){
                return Mage::getBaseUrl().$prefix.$seperator.$result['final_url_key'];
            } else {
                return $seperator.$result['final_url_key'];
            }
        }
        return false;
    }


}
