<?php
/**
 *
 *
 */

class Juno_LandingPages_Model_System_Config_Source_Category
{
    /**
     * Prepare and return array of attributes names.
     */
    public function getAllOptions()
    {
        $options[0] = 'All Categories';

        foreach($this->getCategories() as $category) {
            $options[$category['id']] = $category['name'];
        }
        return $options;
    }

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }

    private function getCategories()
    {
        $read = Mage::getModel('core/resource')->getConnection('core_read');
        $results = $read->fetchAll($read->select()->from(array('e'=>'catalog_category_entity'), '')
                                                  ->joinleft(array('v'=>'catalog_category_entity_varchar'), 'e.entity_id = v.entity_id', array('value as name','entity_id as id'))
                                                  ->where('v.attribute_id = ?', 41)
                                                  ->where('e.level = ?', 2)
                                                  ->order('v.value')
                                        );
        return $results;
    }

}