<?php



class Juno_LandingPages_SitemapController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        header('Content-Type: text/xml');
        $sitemap = array();
        $read = Mage::getModel('core/resource')->getConnection('core_read');

        $base_url = Mage::getBaseUrl().'from-';

        $collection = $read->fetchAll($read->select()->from('m_seo_url')->where('schema_id = ?', 1)->where('type = ?', 'option'));
        foreach($collection as $item){
            $sitemap[] = array('loc'        => $base_url.$item['final_url_key'],
                               'lastmod'    => date('Y-m-d'),
                               'changefreq' => 'daily',
                               'priority'   => '0.5');
        }
        $xml = $this->generateValidXmlFromArray($sitemap, 'urlset', 'url');
        echo $xml;
    }

    public static function generateValidXmlFromArray($array, $node_block='nodes', $node_name='node') {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';

        $xml .= '<' . $node_block . '>';
        $xml .= self::generateXmlFromArray($array, $node_name);
        $xml .= '</' . $node_block . '>';

        return $xml;
    }

    private static function generateXmlFromArray($array, $node_name) {
        $xml = '';

        if (is_array($array) || is_object($array)) {
            foreach ($array as $key=>$value) {
                if (is_numeric($key)) {
                    $key = $node_name;
                }

                $xml .= '<' . $key . '>' . self::generateXmlFromArray($value, $node_name) . '</' . $key . '>';
            }
        } else {
            $xml = htmlspecialchars($array, ENT_QUOTES);
        }

        return $xml;
    }

}