<?php 
/*
Template Name: Press
*/
ini_set('display_errors', 1);
get_header(); ?>
	<div class="breadcrumbs">
	<ul>
	    <li class="home">
		<a href="<?php echo Mage::getUrl(''); ?>" title="Go to Home Page">Home</a>
	    </li>
	    <li class="last">
		<span>Press</span>
	    </li>
	</ul>
    </div>
    <div class="main-container col1-layout press-page">
		<div class="main">		
			<div class="col-main">
				<div class="cms-wrapper">
					<div class="cms-template">
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<h1><?php the_title(); ?></h1>
							<?php the_content(); ?>
						<?php endwhile; endif; ?>
				
					    <ul class="press">
						<?php $press_query = 'post_type=press&posts_per_page=3';
						if ($paged = get_query_var('page')) :
							$press_query .= "&paged=$paged";
						endif;
						$press = new WP_Query($press_query); 
						$i = 0;
						while($press ->have_posts()): $press ->the_post(); $i++
						?>
						    <li>
								<?php the_post_thumbnail('full'); ?>
								<h2><?php the_title(); ?></h2>
								<?php if ($publication_date = get_post_meta($post->ID, 'publication_date', true)) :
									echo '<h3>' . $publication_date . '</h3>';
								endif; ?>
								<?php the_content() ?>
						    </li>
						<?php endwhile; ?>
					    </ul>
			
						<?php $big = 999999999; // need an unlikely integer
			
						$pagination_vars = array(
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, get_query_var('page') ),
							'total' => $press->max_num_pages,
							'prev_text'    => __('Previous'),
							'next_text'    => __('Next')
						);
			
						if ( $pagination = paginate_links( $pagination_vars ) ) :
							?><div class="pagination">Page: <?php echo $pagination; ?></div><?php 
						endif;
			
						?>
						
					</div><!-- .cms-template -->
		    	</div><!-- .cms-wrapper -->
				
		    </div><!-- .col-main -->
	    </div><!-- .main -->
    </div><!-- .main-container -->

<?php get_footer(); ?>