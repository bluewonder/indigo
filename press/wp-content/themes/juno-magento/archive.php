
<?php
get_header(); ?>
    <div id="body">
	<div class="contents">		
	    <div class="breadcrumbs">
		<ul>
		    <li class="home">
			<a href="<?php echo Mage::getUrl(''); ?>" title="Go to Home Page">Home</a>
		    </li>
		    <li class="last">
			<span>Blog</span>
		    </li>
		</ul>
	    </div>
				
	    <div class="col-main">
		<h1 class="page-title">
		    <?php if ( is_day() ) : ?>
			    <?php printf( __( 'Daily Archives: %s' ), '<span>' . get_the_date() . '</span>' ); ?>
		    <?php elseif ( is_month() ) : ?>
			    <?php printf( __( 'Monthly Archives: %s' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format') ) . '</span>' ); ?>
		    <?php elseif ( is_year() ) : ?>
			    <?php printf( __( 'Yearly Archives: %s' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format') ) . '</span>' ); ?>
		    <?php elseif( is_category() ) : ?>
			    <?php echo 'Category: ' . '<span>' . single_cat_title('', false) . '</span>'; ?>
		    <?php else: ?>
			<?php _e( 'Blog Archives', 'twentyeleven' ); ?>
		    <?php endif; ?>
		</h1>
		    <div class="blog-list">
			<ul class="list-post">
			    <?php while(have_posts()): the_post(); ?>
			    <li>
				<?php if(has_post_thumbnail()): ?>
				<a href="<?php the_permalink(); ?>" class="image"><?php the_post_thumbnail('thumb_700_407') ?></a>
				<?php endif; ?>
				<a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a>
				<p class="posted-on">Posted on <?php the_time('F d, Y') ?></p>
				<p><?php echo wp_trim_words($post->post_content, 65); ?></p>
				<a href="<?php the_permalink(); ?>" class="read-more">+ Read</a>
			    </li>
			    <?php endwhile; ?>
			</ul>
			
			<?php wp_pagenavi() ?>
		    </div><!-- .blog-list -->
		    
	    </div><!-- .col-main -->
	    
	    <?php get_sidebar(); ?>
				
	    </div><!-- .contents -->
    </div><!-- #body -->

<?php get_footer(); ?>