Copy a version of app/code/core/Mage/Core/functions.php to app/code/local/Mage/Core/functions.php then wrap:

function __() {... }

in:

if ( !function_exists('__') ) {... }
